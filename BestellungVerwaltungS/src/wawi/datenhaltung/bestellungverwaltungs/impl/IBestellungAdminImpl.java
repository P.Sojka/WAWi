/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.datenhaltung.bestellungverwaltungs.impl;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TemporalType;
import wawi.datenhaltung.wawidbmodel.entities.Bestellung;
import java.util.Date;
import wawi.datenhaltung.bestellungverwaltungs.service.IBestellungAdmin;

/**
 *
 * @author Patrick-Sojka
 */

public class IBestellungAdminImpl implements IBestellungAdmin {
    
    private EntityManager em;
    
    public void setEntityManager(EntityManager em){
        
      this.em = em;  
      
    }
    
    /**
     * Hier wird die Methode für den Dummy getAlleBestellungen ausprogrammiert.Eine gesamte Liste mit allen Bestellungen wird zurückgeliefert.
     * @return blist
     */
    
    @Override
    public List<Bestellung> getAlleBestellungen(){
        
        List<Bestellung> blist;
        
        blist = em.createNamedQuery("Bestellung.findAll").getResultList();
       
        return blist;
    }
    
    /**
     * Hier wird die Methode für den Dummy getBestellungenByDatum ausprogrammiert.Eine gesamte Liste mit allen Bestellung wird zurückgeliefert anhand eines bestimmten Zeitraumes.
     * @param von
     * @param bis
     * @return blist
     */
    
    @Override
    public List<Bestellung> getBestellungenByDatum(Date von,Date bis){
        
        List<Bestellung> blist;
        
        blist = em.createNativeQuery("SELECT * FROM bestellung WHERE created >='"+von+"' AND created <= '"+bis+"'",Bestellung.class).getResultList();
        
        return blist;
        
    }
    
    /**
     * Hier wird die Methode für den Dummy getBestellungById ausprogrammiert.Eine Bestellung wird anhand einer bestellId gesucht und zurückgeliefert.
     * @param bid
     * @return bestellung
     */
    @Override    
    public Bestellung getBestellungById(int bid){
        
        Bestellung bestellung = null;
        if (bid > 0) {
            bestellung = em.find(Bestellung.class, bid);
        }
        return bestellung;
    }

         
}
