/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.datenhaltung.bestellungverwaltungs.service;

import java.util.*;
import javax.persistence.EntityManager;
import wawi.datenhaltung.wawidbmodel.entities.Bestellung;
/**
 *
 * @author Patrick-Sojka
 */
public interface IBestellungAdmin {

    
    public void setEntityManager(EntityManager em);
    
    public List<Bestellung> getAlleBestellungen();
    
    public List<Bestellung> getBestellungenByDatum(Date von,Date bis);
            
    public Bestellung getBestellungById(int bid);
    
}

