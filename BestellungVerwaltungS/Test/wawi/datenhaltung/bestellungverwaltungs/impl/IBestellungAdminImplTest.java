/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.datenhaltung.bestellungverwaltungs.impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import wawi.datenhaltung.wawidbmodel.entities.Bestellung;
import wawi.datenhaltung.wawidbmodel.impl.IDatabaseImpl;
import wawi.datenhaltung.wawidbmodel.service.EntityManagerSingleton;

/**
 *
 * @author gromow
 */
public class IBestellungAdminImplTest {
    
    private static EntityManagerSingleton ems;
    private static  EntityManager em;
    private IBestellungAdminImpl classUnderTest;
    private static IDatabaseImpl IDataImpl;
    
    public IBestellungAdminImplTest() {
    }

    /**
     * To solve the second issue, we must guarantee that each test fixture gets a fresh entity manager.
     * JUnit 4+ executes whatever public static method annotated with @BeforeClass before firing up the class constructor.
     * Similarly, methods annotated with @AfterClass are executed after all the tests and is therefore a good place for cleanup.
     */
    @BeforeClass
    public static void testOpenSetEntityManager(){
        ems = EntityManagerSingleton.getInstance();
    }
    
    /**
     * Es ist eigentlich zulässig bei @Before die Instanziierung IDataImpl komplett zu unterdrücken, weil dank der @BeforeClass wird
     * em jedes mal neu geholt und verwendet.Mit @AfterClass zwinge ich das em zuzugehen.
     */
    @Before
    public void angenommen() {
        IDataImpl = new IDatabaseImpl();
        //IDataImpl.useProdPU();
        IDataImpl.useDevPU();                // Dev Datenbank verwenden 
        em = IDataImpl.getEntityManager();   // das veränderte EntityManager rausholen
        //em = ems.getEntityManager(); // Nur wenn ich es mit @BeforeClass und @AfterClass ausführen möchte
        classUnderTest = new IBestellungAdminImpl();
        classUnderTest.setEntityManager(em);
        em.getTransaction().begin();
    }
    
    /**
     * AM ENDE wird die Transaktion zurück gesetzt.
     */
    @After
    public void amEnde() {
        em.getTransaction().rollback();
    }

    /**
     * Test of getAlleBestellungen method, of class IBestellungAdminImpl.
     */
    @Test
    public void testGetAlleBestellungen() {
        List<Bestellung> blist = classUnderTest.getAlleBestellungen();
        assertNotEquals(blist.size(), 0);
        System.out.println(blist.size());
        
    }

    /**
     * Test of getBestellungenById method, of class IBestellungAdminImpl.
     */
    @Test
    public void testGetBestellungenById() {
        Bestellung b = classUnderTest.getBestellungenById(1);
        assertNotNull(b);
        System.out.println(b.getLieferadresse());
    }

    /**
     * Test of getBestellungenByDatum method, of class IBestellungAdminImpl.
     */
    @Test
    public void testGetBestellungenByDatum() throws ParseException {
        String von = "2015-08-10";
        String bis = "2015-08-12";
        
        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        
        Date datevon = formatter.parse(von);
        Date datebis = formatter.parse(bis);
        //Ein Tag dazuaddieren
        Calendar c = Calendar.getInstance();
        c.setTime(datebis);
        c.add(Calendar.DATE, 1);
        datebis = c.getTime();
        //liefert sql Dateformat YYYY-MM-DD zurück
        java.sql.Date sqlDateVon = new java.sql.Date(datevon.getTime());// liefert wieder 2015-08-10
        java.sql.Date sqlDateBis = new java.sql.Date(datebis.getTime());// liefert wieder 2015-08-12
        //holt die Liste mit dem Interval
        List<Bestellung> blist = classUnderTest.getBestellungenByDatum(sqlDateVon, sqlDateBis);
        assertNotEquals(blist.size(), 0);//sol 2 Liefer
        System.out.println(blist.size());
    }
    
     /**
     * Test of testCloseSetEntityManager methodto close existing em.
     */
    @AfterClass
    public static void testCloseSetEntityManager(){
        em.close();
    }
    
}
