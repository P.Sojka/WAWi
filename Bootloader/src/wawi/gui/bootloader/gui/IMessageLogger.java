package wawi.gui.bootloader.gui;

import wawi.gui.bootloader.grenz.Message;

/**
 * Interface for logging messages.
 * @author farhan
 */
public interface IMessageLogger {
    public void logMessage(String msg, Message msgTyp);    
}
