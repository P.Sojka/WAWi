package wawi.gui.bootloader.grenz;

/**
 *
 * @author farhan
 */
public enum Message {
    Error,
    Ok,
    Info,
    Warning,
    Clear,    
    LoginError
}
