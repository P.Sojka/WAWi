package wawi.datenhaltung.wawidbmodel.service;

import javax.persistence.EntityManager;

/**
 *
 * @author moetz
 */
public interface IDatabase {	
	public EntityManager getEntityManager();
	public void useDevPU();
	public void useProdPU();
	public String getCurrentPU();
}
