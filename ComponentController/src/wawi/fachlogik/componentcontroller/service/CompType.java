package wawi.fachlogik.componentcontroller.service;

public enum CompType {
    COMPONENTCONTROLLER,
    KUNDE,
    ADMIN,
    SACHBEARBEITER,
    LAGERHALTER
}
