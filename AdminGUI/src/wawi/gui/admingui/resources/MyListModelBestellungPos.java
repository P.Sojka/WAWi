/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.gui.admingui.resources;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.persistence.EntityManager;
import javax.swing.table.DefaultTableModel;
import wawi.datenhaltung.bestellungverwaltungs.impl.IBestellungAdminImpl;
import wawi.datenhaltung.bestellungverwaltungs.service.IBestellungAdmin;
import wawi.datenhaltung.wawidbmodel.entities.Bestellungsposition;
import wawi.datenhaltung.wawidbmodel.impl.IDatabaseImpl;

/**
 *
 * @author gromow
 */
public class MyListModelBestellungPos {
    private static IDatabaseImpl IDataImpl;
    private static EntityManager em;
    private static List<Bestellungsposition> bestellungsposlist;
    private static Bestellungsposition bsp;
    private static int bestellungID;

    public static void setBestellungID(int bestellungID) {
        MyListModelBestellungPos.bestellungID = bestellungID;
    }


    public static DefaultTableModel buildTableModel() {

        /* DB */
        IDataImpl = new IDatabaseImpl();
        //IDataImpl.useProdPU();
        IDataImpl.useDevPU();// Dev Datenbank verwenden 
        em = IDataImpl.getEntityManager();
        IBestellungAdmin iba = new IBestellungAdminImpl();
        iba.setEntityManager(em);
        em.getTransaction().begin(); 
        bestellungsposlist = em.createNativeQuery("SELECT * FROM bestellungsposition WHERE bestellung ="+bestellungID,Bestellungsposition.class).getResultList();
        em.getTransaction().commit();
        /* Ende DB */

        // names of columns
        Vector<String> columnNames = new Vector<String>();

        columnNames.add("ID");
        columnNames.add("Bestellung");
        columnNames.add("Produkt");
        columnNames.add("Anzahl");


        // data of the table
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        //while (rs.next()) {
        Vector<Object> vector = new Vector<Object>();
        
        Iterator<Bestellungsposition> BestellungsposIterator = bestellungsposlist.iterator();
        
        while (BestellungsposIterator.hasNext()) {
            vector = new Vector<Object>();
            bsp = BestellungsposIterator.next();
            vector.add(bsp.getBpid());
            vector.add(bsp.getBestellung().getBid());
            vector.add(bsp.getProdukt().getName());
            vector.add(bsp.getAnzahl());

            data.add(vector);
        }

        
        //}

        return new DefaultTableModel(data, columnNames);

    }
}
