/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.gui.admingui.resources;


import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.swing.DefaultComboBoxModel;
import wawi.datenhaltung.wawidbmodel.entities.Kategorie;
import wawi.fachlogik.adminsteuerung.grenz.KategorieGrenz;
import wawi.fachlogik.adminsteuerung.steuer.KategorieSteuerung;
import wawi.gui.admingui.resources.Item;

/**
 * http://www.java2s.com/Code/JavaAPI/javax.swing/newJListListModeldataModelSharemodelwithJComboBox.htm
 * http://stackoverflow.com/questions/5374311/convert-arrayliststring-to-string-array
 * @author Patrick-Sojka
 */
public class MyModelComboKategorie {

    /**
     * Diese Methode ist dafür da, damit man Produkte jeder Kategorie auch Ober zuweisen kann
     * @return 
     */
    public DefaultComboBoxModel buildTableModel(){
        KategorieSteuerung kategoriesteuerung = new KategorieSteuerung();
        List<KategorieGrenz> katgrenzlist = kategoriesteuerung.kategorieAnzeigen();

        Iterator<KategorieGrenz> kategoriegrenzIterator = katgrenzlist.iterator();
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        
        while(kategoriegrenzIterator.hasNext()){
            KategorieGrenz kg = kategoriegrenzIterator.next();
            //if(kg.getParentkatid()!=0){
            model.addElement(new Item(kg.getKatid(), kg.getName()));
            //}
        }
        return model;
    }
    
    /**
     * Diese Methode ist für das Anlegen einer Unterkategorie der Oberkategorie
     * @param a
     * @return 
     */
    public DefaultComboBoxModel buildTableModel(boolean a){
        KategorieSteuerung kategoriesteuerung = new KategorieSteuerung();
        List<KategorieGrenz> katgrenzlist = kategoriesteuerung.kategorieAnzeigen();

        Iterator<KategorieGrenz> kategoriegrenzIterator = katgrenzlist.iterator();
        DefaultComboBoxModel model = new DefaultComboBoxModel();
        model.addElement(new Item(0, "keine"));
        while(kategoriegrenzIterator.hasNext()){
            KategorieGrenz kg = kategoriegrenzIterator.next();
            if(kg.getParentkatid()==kg.getKatid() || kg.getParentkatid()==0){
                model.addElement(new Item(kg.getKatid(), kg.getName()));
            }
            
        }   
        return model;
    }
}

