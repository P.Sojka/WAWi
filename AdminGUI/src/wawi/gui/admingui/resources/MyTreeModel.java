/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.gui.admingui.resources;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import wawi.datenhaltung.produktverwaltung.impl.ICRUDKategorieImpl;
import wawi.datenhaltung.produktverwaltung.service.ICRUDKategorie;
import wawi.datenhaltung.wawidbmodel.entities.Kategorie;
import wawi.datenhaltung.wawidbmodel.entities.Produkt;
import wawi.datenhaltung.wawidbmodel.impl.IDatabaseImpl;

/**
 * https://docs.oracle.com/javase/tutorial/displayCode.html?code=https://docs.oracle.com/javase/tutorial/uiswing/examples/components/TreeDemoProject/src/components/TreeDemo.java
 *
 * @author gromow
 */
public class MyTreeModel {

    private static IDatabaseImpl IDataImpl;
    private static EntityManager em;
    private static CustomUserObject uObj;
    private static DefaultMutableTreeNode subroot;
    private static DefaultMutableTreeNode root;
    private static DefaultMutableTreeNode parent;
    private static int oberkategorie;
    

    public static javax.swing.tree.TreeModel getDefaultTreeModel() {

        IDataImpl = new IDatabaseImpl();

        em = IDataImpl.getEntityManager();
        ICRUDKategorie ick = new ICRUDKategorieImpl();
        ick.setEntityManager(em);

        List<Kategorie> klist = ick.getKategorieListe();

        /* SORTIERUNG */
        List<Kategorie> klist2 = new ArrayList<>();
        Iterator<Kategorie> kategorieIterator = klist.iterator();
        
        root = new DefaultMutableTreeNode(new CustomUserObject(1000, "WAWI-Kategorien", null));
        
        //root.add(new DefaultMutableTreeNode(new CustomUserObject(0, "keine", "none")));
        //Anfangswert ID da in der DB eine 1 als Anfangswert in der ParentKatid ist
        int cc = 1;
        for (int k = 0; k < klist.size(); k++) {

                if(klist.get(k).getKatid() ==  klist.get(k).getParentkatid() || klist.get(k).getParentkatid()==0){
                oberkategorie = klist.get(k).getKatid();
                uObj = new CustomUserObject(klist.get(k).getKatid(), klist.get(k).getName(), "kat");
                subroot = new DefaultMutableTreeNode(uObj);
                
                    for (int d = 0; d < klist.size(); d++) {
                        if (klist.get(d).getParentkatid() == oberkategorie && klist.get(d).getParentkatid()!=klist.get(d).getKatid()) {
                        uObj = new CustomUserObject(klist.get(d).getKatid(), klist.get(d).getName(), "untkat");
                        parent = new DefaultMutableTreeNode(uObj);
                        subroot.add(parent); 
                        }
                    }

              root.add(subroot);
            }
            
            cc++;
        }
        return new DefaultTreeModel(root);
    }
    
    
}
