/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.gui.admingui.resources;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import wawi.fachlogik.adminsteuerung.grenz.LagerverkehrGrenz;

/**
 *
 * @author gromow
 */
public class MyListModelLagerVerkegrShow {

    private int lid;
    private List<LagerverkehrGrenz> lagerverkehrgrenzlist;

    public MyListModelLagerVerkegrShow() {
    }

    public MyListModelLagerVerkegrShow(int lid, List<LagerverkehrGrenz> lagerverkehrgrenzlist) {
        this.lid = lid;
        this.lagerverkehrgrenzlist = lagerverkehrgrenzlist;
    }
    
    

    public List<LagerverkehrGrenz> getLagerverkehrgrenzlist() {
        return lagerverkehrgrenzlist;
    }

    public void setLagerverkehrgrenzlist(List<LagerverkehrGrenz> lagerverkehrgrenzlist) {
        this.lagerverkehrgrenzlist = lagerverkehrgrenzlist;
    }

    public int getLid() {
        return lid;
    }

    public void setLid(int lid) {
        this.lid = lid;
    }



    public DefaultTableModel buildTableModel() {
        
        // names of columns
        Vector<String> columnNames = new Vector<String>();

        columnNames.add("ID");
        columnNames.add("Lager");
        columnNames.add("Erstellt am");
        columnNames.add("Einlieferung / Bestellung");
        columnNames.add("Gesamtpreis");

        // data of the table
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        //while (rs.next()) {
        Vector<Object> vector = new Vector<Object>();

        Iterator<LagerverkehrGrenz> LagerVerkehrGrenzIterator = lagerverkehrgrenzlist.iterator();
        while (LagerVerkehrGrenzIterator.hasNext()) {

            vector = new Vector<Object>();
            LagerverkehrGrenz lagerverkehrgrenzlist = LagerVerkehrGrenzIterator.next();
            
            vector.add(lagerverkehrgrenzlist.getLgvid());
            vector.add(lagerverkehrgrenzlist.getLager());
            
            DateFormat formatter=DateFormat.getDateInstance(DateFormat.SHORT, new Locale("de","DE"));
            vector.add(formatter.format(lagerverkehrgrenzlist.getCreated()));
            
            /************  Bestelungen / Einlieferung ************/
            
//            try {
//                int bid = lagerverkehrgrenzlist.getBestellung();
//                ausgabe = "Bestellung";
//            } catch (NullPointerException e) {
//                
//            }
//            try {
//                int elfid = lagerverkehrgrenzlist.getEinlieferung();
//                ausgabe = "Einlieferung";
//            } catch (NullPointerException e) {
//                
//            }
            String ausgabe = "--";
            if(lagerverkehrgrenzlist.getBestellung()==0){
                ausgabe="Einlieferung";
            }
            if(lagerverkehrgrenzlist.getEinlieferung()==0){
                ausgabe="Bestellung";
            }
            if(lagerverkehrgrenzlist.getBestellung()==0 && lagerverkehrgrenzlist.getEinlieferung()==0){
                ausgabe="--";
            }

            vector.add(ausgabe);
            /************  Bestelungen / Einlieferung ENDE ************/
            
            /************  GesamtPreis  / GesamtBrutto  ************/
//            String gesamt = "--";
//            try {
//                BigDecimal gesamtpreiseinl = lagerverkehrgrenzlist.getGesamtpreis();
//                gesamt = gesamtpreiseinl+" €";
//            } catch (NullPointerException e) {
//                
//            }
//            
//            try {
//                BigDecimal gesamtpreisbest = lagerverkehrgrenzlist.getBestellung().getGesamtbrutto();
//                gesamt=gesamtpreisbest+" €";
//            } catch (NullPointerException e) {
//                
//            }
            /************  GesamtPreis  / GesamtBrutto  ENDE ************/
            BigDecimal gesamtpreis;
            
            
            if(lagerverkehrgrenzlist.getGesamtpreis() == null){
                vector.add("0.00 €");
            }else{
                gesamtpreis = lagerverkehrgrenzlist.getGesamtpreis();
                vector.add(gesamtpreis + " €");
            }
            
            
            data.add(vector);

        }

        //}
        return new DefaultTableModel(data, columnNames);

    }
}
