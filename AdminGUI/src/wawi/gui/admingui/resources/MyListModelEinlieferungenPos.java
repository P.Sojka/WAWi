/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.gui.admingui.resources;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import wawi.datenhaltung.wawidbmodel.entities.Lieferposition;
import wawi.fachlogik.adminsteuerung.grenz.EinlieferungGrenz;
import wawi.fachlogik.adminsteuerung.grenz.LieferpositionGrenz;

/**
 *
 * @author gromow
 */
public class MyListModelEinlieferungenPos {

    private static EinlieferungGrenz einlieferunggrenz;

    public MyListModelEinlieferungenPos() {
    }

    public static EinlieferungGrenz getEinlieferunggrenz() {
        return einlieferunggrenz;
    }

    public static void setEinlieferunggrenz(EinlieferungGrenz einlieferunggrenz) {
        MyListModelEinlieferungenPos.einlieferunggrenz = einlieferunggrenz;
        
    }

    public DefaultTableModel buildTableModel() {

        // names of columns
        Vector<String> columnNames = new Vector<String>();

        columnNames.add("ID");
        columnNames.add("Einlieferung");
        columnNames.add("Produkt");
        columnNames.add("Anzahl");
        columnNames.add("Kaufpreis");

        Vector<Vector<Object>> data = new Vector<Vector<Object>>();

        Vector<Object> vector = new Vector<Object>();

        
        
        Iterator<LieferpositionGrenz> lieferpositiongrenzIterator = einlieferunggrenz.getLipos().iterator();
        while (lieferpositiongrenzIterator.hasNext()) {
            
            LieferpositionGrenz lieferPositionGrenz = lieferpositiongrenzIterator.next();
            if(einlieferunggrenz.getElfid()==lieferPositionGrenz.getEinlieferung()){
            vector = new Vector<Object>();

            vector.add(lieferPositionGrenz.getLpid());
            vector.add(lieferPositionGrenz.getEinlieferung());
            vector.add(lieferPositionGrenz.getProdukt());
            vector.add(lieferPositionGrenz.getAnzahl());
            try {
                vector.add(lieferPositionGrenz.getKaufpreis() + " €");
            } catch (Exception e) {
                vector.add("--");
            }
            

            data.add(vector);
            }
        }

//            try {
//               //Todo...
//            } catch (NullPointerException e) {
//                //vector.add("");
//            }
        return new DefaultTableModel(data, columnNames);

    }
}
