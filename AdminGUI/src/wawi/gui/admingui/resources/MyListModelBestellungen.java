/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.gui.admingui.resources;

import java.text.DateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import wawi.fachlogik.adminsteuerung.grenz.BestellungGrenz;

/**
 *
 * @author gromow
 */
public class MyListModelBestellungen {

    private int bid;
    private List<BestellungGrenz> bestellunggrenzlist;

    public void setBid(int bid) {
        this.bid = bid;
    }

    public void setBestellunggrenzlist(List<BestellungGrenz> bestellunggrenzlist) {
        this.bestellunggrenzlist = bestellunggrenzlist;
    }

    public int getBid() {
        return bid;
    }

    public List<BestellungGrenz> getBestellunggrenzlist() {
        return bestellunggrenzlist;
    }

    public DefaultTableModel buildTableModel() {

        // names of columns
        Vector<String> columnNames = new Vector<String>();

        columnNames.add("ID");
        columnNames.add("Kunde");
        columnNames.add("Lieferadresse");
        columnNames.add("Rechnungsadresse");
        columnNames.add("Erstellt am");
        columnNames.add("Status");
        columnNames.add("Gesamtnetto");
        columnNames.add("Gesamtbrutto");

        // data of the table
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        //while (rs.next()) {
        Vector<Object> vector = new Vector<Object>();

        Iterator<BestellungGrenz> BestellungGrenzIterator = bestellunggrenzlist.iterator();
        while (BestellungGrenzIterator.hasNext()) {

            vector = new Vector<Object>();
            BestellungGrenz bestellunggrenz = BestellungGrenzIterator.next();
            vector.add(bestellunggrenz.getBid());
            vector.add(bestellunggrenz.getKunde());
            vector.add(bestellunggrenz.getLieferadresse());
            vector.add(bestellunggrenz.getRechnungsadresse());
            
            DateFormat formatter=DateFormat.getDateInstance(DateFormat.SHORT, new Locale("de","DE"));
            
            vector.add(formatter.format(bestellunggrenz.getCreated()));
            vector.add(bestellunggrenz.getStatus());
            vector.add(bestellunggrenz.getGesamtnetto());
            vector.add(bestellunggrenz.getGesamtbrutto());

            data.add(vector);

        }

        //}
        return new DefaultTableModel(data, columnNames);

    }
}
