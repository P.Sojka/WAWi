/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.gui.admingui.resources;

import java.text.DateFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import wawi.fachlogik.adminsteuerung.grenz.EinlieferungGrenz;

/**
 *
 * @author gromow
 */
public class MyListModelEinlieferungenShow {

    private int eid;
    private List<EinlieferungGrenz> einlieferunggrenzlist;

    public MyListModelEinlieferungenShow() {
    }

    public MyListModelEinlieferungenShow(int eid, List<EinlieferungGrenz> einlieferunggrenzlist) {
        this.eid = eid;
        this.einlieferunggrenzlist = einlieferunggrenzlist;
    }

    public int getEid() {
        return eid;
    }

    public List<EinlieferungGrenz> getEinlieferunggrenzlist() {
        return einlieferunggrenzlist;
    }

    public void setEid(int eid) {
        this.eid = eid;
    }

    public void setEinlieferunggrenzlist(List<EinlieferungGrenz> einlieferunggrenzlist) {
        this.einlieferunggrenzlist = einlieferunggrenzlist;
    }





    public DefaultTableModel buildTableModel() {
        
        // names of columns
        Vector<String> columnNames = new Vector<String>();

        columnNames.add("ID");
        columnNames.add("Lieferant");
        columnNames.add("Gesamtpreis");
        columnNames.add("Erstellt am");

        // data of the table
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        //while (rs.next()) {
        Vector<Object> vector = new Vector<Object>();

        Iterator<EinlieferungGrenz> EinlieferungGrenzIterator = einlieferunggrenzlist.iterator();
        while (EinlieferungGrenzIterator.hasNext()) {

            vector = new Vector<Object>();
            EinlieferungGrenz einlieferunggrenz = EinlieferungGrenzIterator.next();
            
            vector.add(einlieferunggrenz.getElfid());
            vector.add(einlieferunggrenz.getLieferant());
            //vector.add(einlieferunggrenz.getLieferan());//unnötig da das Objekt Lieferant bleibt immer in der GRenzListe
            
            try {
                vector.add(einlieferunggrenz.getGesamtpreis()+" €");
            } catch (NullPointerException e) {
                vector.add("--");
            }
            DateFormat formatter=DateFormat.getDateInstance(DateFormat.SHORT, new Locale("de","DE"));
            vector.add(formatter.format(einlieferunggrenz.getCreated()));
            
            data.add(vector);

        }

        //}
        return new DefaultTableModel(data, columnNames);

    }
}
