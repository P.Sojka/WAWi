/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.gui.admingui.resources;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import wawi.fachlogik.adminsteuerung.grenz.ProduktGrenz;

/**
 *
 * @author Patrick-Sojka
 */

public class MyListModel {

    private int pid;
    private List<ProduktGrenz> produktgrenzlist;

    public void setProduktid(int pid) {
        this.pid = pid;
    }

    public void setProduktgrenzlist(List<ProduktGrenz> produktgrenzlist) {
        this.produktgrenzlist = produktgrenzlist;
    }

    public int getProduktid() {
        return pid;
    }

    public List<ProduktGrenz> getProduktgrenzlist() {
        return produktgrenzlist;
    }

    public DefaultTableModel buildTableModel() {

        // names of columns
        Vector<String> columnNames = new Vector<String>();

        columnNames.add("ID");
        columnNames.add("Produktname");
        columnNames.add("Status");

        // data of the table
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        //while (rs.next()) {
        Vector<Object> vector = new Vector<Object>();

        Iterator<ProduktGrenz> ProduktGrenzIterator = produktgrenzlist.iterator();
        while (ProduktGrenzIterator.hasNext()) {

            vector = new Vector<Object>();
            ProduktGrenz produktgrenz = ProduktGrenzIterator.next();
            vector.add(produktgrenz.getProdid());
            vector.add(produktgrenz.getName());
            vector.add(produktgrenz.isAktiv());



            data.add(vector);

        }

        //}
        return new DefaultTableModel(data, columnNames);

    }
}
