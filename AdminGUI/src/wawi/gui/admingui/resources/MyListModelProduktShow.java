/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.gui.admingui.resources;

import java.util.Iterator;
import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import wawi.datenhaltung.wawidbmodel.entities.Produkt;
import wawi.fachlogik.adminsteuerung.grenz.ProduktGrenz;
import wawi.gui.admingui.gui.FrameProduktShow;

/**
 *
 * @author Anton Li
 */
public class MyListModelProduktShow {

    private List<ProduktGrenz> produktgrenzlist;
    Produkt produkt = new Produkt();

    public void setProduktgrenzlist(List<ProduktGrenz> produktgrenzlist) {
        this.produktgrenzlist = produktgrenzlist;
        FrameProduktShow.setProduktgrenzlisteAktivDeaktiv(produktgrenzlist);

    }

    public List<ProduktGrenz> getProduktgrenzlist() {
        return produktgrenzlist;
    }

    public DefaultTableModel buildTableModel() {

        // names of columns
        Vector<String> columnNames = new Vector<String>();

        columnNames.add("ID");
        columnNames.add("Name");
        columnNames.add("Beschreibung");
        columnNames.add("Nettopreis");
        columnNames.add("MWSTsatz");
        columnNames.add("Stückzahl");
        columnNames.add("Lagerort");
        columnNames.add("Kategorie");
        columnNames.add("Status");

        // data of the table
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        //while (rs.next()) {
        Vector<Object> vector = new Vector<Object>();

        Iterator<ProduktGrenz> ProduktGrenzIterator = produktgrenzlist.iterator();
        while (ProduktGrenzIterator.hasNext()) {

            vector = new Vector<Object>();
            ProduktGrenz produktgrenz = ProduktGrenzIterator.next();

            vector.add(produktgrenz.getProdid());
            vector.add(produktgrenz.getName());
            vector.add(produktgrenz.getBeschreibung());
            vector.add(produktgrenz.getNettopreis());
            vector.add(produktgrenz.getMwstsatz());
            vector.add(produktgrenz.getStueckzahl());
            vector.add(produktgrenz.getLagerort());
            vector.add(produktgrenz.getKategorie());
            vector.add((boolean) produktgrenz.getAktiv());

            data.add(vector);

        }

        return new DefaultTableModel(data, columnNames);

    }
}
