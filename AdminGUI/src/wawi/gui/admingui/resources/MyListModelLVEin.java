/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.gui.admingui.resources;


import java.text.DateFormat;
import java.util.Locale;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import wawi.fachlogik.adminsteuerung.grenz.EinlieferungGrenz;
import wawi.fachlogik.adminsteuerung.grenz.LagerverkehrGrenz;
import wawi.fachlogik.adminsteuerung.steuer.LagerSteuerung;

/**
 *
 * @author gromow
 */
public class MyListModelLVEin {
    private static LagerverkehrGrenz lagerverkehrgrenz;

    public void setLagerverkehrgrenz(LagerverkehrGrenz lagerverkehrgrenz) {
        this.lagerverkehrgrenz = lagerverkehrgrenz;
    }
    
    public DefaultTableModel buildTableModel() {
        
        // names of columns
        Vector<String> columnNames = new Vector<String>();

        columnNames.add("ID");
        columnNames.add("Lieferant");
        columnNames.add("Erstellt am");
        columnNames.add("Gesamtpreis");

        Vector<Vector<Object>> data = new Vector<Vector<Object>>();

        Vector<Object> vector = new Vector<Object>();
           

            vector = new Vector<Object>();

            try {
                int elfid = lagerverkehrgrenz.getEinlieferung();
                LagerSteuerung ls = new LagerSteuerung();
                EinlieferungGrenz einlieferunggrenz = ls.einlieferungenAnzeigen(elfid);
                vector.add(einlieferunggrenz.getElfid());
                vector.add(einlieferunggrenz.getLieferant());
                DateFormat formatter=DateFormat.getDateInstance(DateFormat.SHORT, new Locale("de","DE"));
                vector.add(formatter.format(einlieferunggrenz.getCreated()));
                vector.add(einlieferunggrenz.getGesamtpreis() +" €");
            } catch (NullPointerException e) {
                vector.add("");
            }
            
            
            
            
            data.add(vector);

        return new DefaultTableModel(data, columnNames);

    }
}
