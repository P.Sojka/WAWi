/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.gui.admingui.resources;

import java.io.Serializable;

/**
 *
 * @author gromow
 */
public class CustomUserObject implements Serializable{
    private int Id = 0;
    private String Title = null;
    private String Type = null;

    public CustomUserObject(int id, String title,String type) {

        this.Id = id;
        this.Title = title;
        this.Type = type;
    }

    public CustomUserObject() {

    }

    public int getId() {
        return this.Id;
    }

    public String getTitle() {
        return this.Title;
    }

    public void setId(int id) {
        this.Id = id;
    }

    public void setSutTitle(String title) {
        this.Title = title;
    }

    public String getType() {
        return Type;
    }

    public void setType(String Type) {
        this.Type = Type;
    }
    
    

    @Override
    public String toString() {
        return this.Title;

    }  
}
