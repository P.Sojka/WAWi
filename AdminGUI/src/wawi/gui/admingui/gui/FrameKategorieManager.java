package wawi.gui.admingui.gui;

import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import static javax.swing.JOptionPane.showMessageDialog;
import javax.swing.JScrollPane;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import wawi.fachlogik.adminsteuerung.grenz.KategorieGrenz;
import wawi.fachlogik.adminsteuerung.steuer.KategorieSteuerung;
import wawi.fachlogik.adminsteuerung.steuer.ProduktSteuerung;
import wawi.gui.admingui.resources.CustomUserObject;
import wawi.gui.admingui.resources.MyTreeModel;

/**
 *
 * @author gromow
 */
public class FrameKategorieManager extends javax.swing.JFrame {

    private FrameKategorieCreate framekategoriecreate;
    private FrameKategorieEdit framekategorieedit;
    private int hiddenValueForKategorie;
    private String hiddenItemType = null;

    //private DefaultMutableTreeNode tnode;
    /**
     * Creates new form FrameKategorieManager
     */
    public FrameKategorieManager() {
        initComponents();
    }

    public static void setjLabel3(String text) {
        FrameKategorieManager.jLabel3.setText(text);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        kategorieCreate = new javax.swing.JButton();
        kategorieEdit = new javax.swing.JButton();
        kategorieDelete = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        close = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setPreferredSize(new java.awt.Dimension(600, 400));

        jLabel1.setFont(new java.awt.Font("Lucida Grande", 1, 13)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel1.setText("Produktkategorien verwalten");

        kategorieCreate.setText("Produktkategorie anlegen");
        kategorieCreate.setSize(new java.awt.Dimension(97, 60));
        kategorieCreate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kategorieCreateActionPerformed(evt);
            }
        });

        kategorieEdit.setText("Produktkategorie bearbeiten");
        kategorieEdit.setSize(new java.awt.Dimension(97, 60));
        kategorieEdit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kategorieEditActionPerformed(evt);
            }
        });

        kategorieDelete.setText("Produktkategorie löschen");
        kategorieDelete.setPreferredSize(new java.awt.Dimension(205, 60));
        kategorieDelete.setSize(new java.awt.Dimension(97, 60));
        kategorieDelete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kategorieDeleteActionPerformed(evt);
            }
        });

        jLabel2.setText("Ausgabe:");

        DefaultMutableTreeNode      root = new DefaultMutableTreeNode("WAWI-Kategorien");
        DefaultMutableTreeNode      parent;
        MyTreeModel mtm = new MyTreeModel();
        jTree1.setModel(mtm.getDefaultTreeModel());
        jTree1.addTreeSelectionListener(new TreeSelectionListener(){
            public void valueChanged(TreeSelectionEvent e) {
                DefaultMutableTreeNode node = (DefaultMutableTreeNode)jTree1.getLastSelectedPathComponent();

                CustomUserObject cuo = (CustomUserObject)node.getUserObject();

                //uniq Nummer
                //hasCode = node.getUserObject().hashCode();
                //kastet jedes Node und holt aus jedem Object die ID
                int itemID = cuo.getId();
                //kastet jedes Node und holt aus jedem Object den Titel
                String itemName = cuo.getTitle();
                String itemType = cuo.getType();
                //hiddenValueForKategorie setzen, damit man deletKategorie ausführen kann
                hiddenValueForKategorie = itemID;
                hiddenItemType = itemType;
                //Ausgabe
                jLabel3.setForeground(Color.BLACK);
                jLabel3.setText("Ausgewählt Kategorie: "+hiddenValueForKategorie);

            }
        });
        //jTree1.remove(index);
        jScrollPane2.setViewportView(jTree1);

        close.setText("Schließen");
        close.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                closeActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addComponent(kategorieDelete, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(kategorieEdit, javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(kategorieCreate, javax.swing.GroupLayout.Alignment.LEADING))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(close)
                        .addGap(6, 6, 6)))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {kategorieCreate, kategorieDelete, kategorieEdit});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 317, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(18, 18, 18)
                        .addComponent(kategorieCreate)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(kategorieEdit)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(kategorieDelete, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(close)
                    .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(16, 16, 16))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {kategorieCreate, kategorieDelete, kategorieEdit});

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void kategorieCreateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kategorieCreateActionPerformed
        framekategoriecreate = new FrameKategorieCreate();
        framekategoriecreate.setTitle("Kategorie anlegen");
        framekategoriecreate.setVisible(true);

    }//GEN-LAST:event_kategorieCreateActionPerformed

    private void closeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_closeActionPerformed
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        dispose();
    }//GEN-LAST:event_closeActionPerformed

    private void kategorieDeleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kategorieDeleteActionPerformed

        boolean ret = false;
        if (hiddenValueForKategorie == 0) {
            showMessageDialog(null, "Bitte Kategorie auswählen");
            jLabel3.setForeground(Color.red);
            jLabel3.setText("Fehler!");
        } else if (hiddenItemType.equals("prod")) {
            showMessageDialog(null, "Bitte Kategorie auswählen");
            jLabel3.setForeground(Color.red);
            jLabel3.setText("Fehler!");
        } else {
            KategorieSteuerung kategoriesteuerung = new KategorieSteuerung();
            ProduktSteuerung produktsteuerung = new ProduktSteuerung();

            KategorieGrenz kategorieGrenz = kategoriesteuerung.kategorieFind(hiddenValueForKategorie);
            
            if (kategorieGrenz.getParentkatid() != 0) {
                int dialogButton = JOptionPane.YES_NO_OPTION;

                dialogButton = JOptionPane.showConfirmDialog(null, "Möchten Sie ausgewähle Kategorie wirklich löschen?", "Hinweis", dialogButton);
                if (dialogButton == JOptionPane.YES_OPTION) { //The ISSUE is here
                    ret = kategoriesteuerung.kategorieLoeschen(hiddenValueForKategorie);
                }
                if (ret == false) {
                    jLabel3.setForeground(Color.red);
                    jLabel3.setText("Fehler. Die Kategorie enthält mindestens ein Produkt.");
                    showMessageDialog(null, "Kann nicht gelöscht werden!");
                    //jTree1.repaint();
                } else {
                    jLabel3.setForeground(Color.BLACK);
                    jLabel3.setText("Erfolgreich gelöscht");
                    hiddenValueForKategorie = 0;
                    hiddenItemType = null;
                }
                
            } else {
                int dialogButton = JOptionPane.YES_NO_OPTION;

                dialogButton = JOptionPane.showConfirmDialog(null, "Möchten Sie wirklich die Oberkategorie löschen?", "Hinweis", dialogButton);
                if (dialogButton == JOptionPane.YES_OPTION) { //The ISSUE is here
                    ret = kategoriesteuerung.kategorieLoeschen(hiddenValueForKategorie);
                    if (ret) {
                        jLabel3.setForeground(Color.BLACK);
                        jLabel3.setText("Erfolgreich gelöscht");
                    } else {
                        jLabel3.setForeground(Color.red);
                        jLabel3.setText("Die Oberkategorie konnte leider nicht gelöscht werden!");
                    }
                }
            }

        }
    }//GEN-LAST:event_kategorieDeleteActionPerformed

    private void kategorieEditActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_kategorieEditActionPerformed
        if (hiddenValueForKategorie == 0) {
            showMessageDialog(null, "Bitte Kategorie auswählen");
            jLabel3.setText("Fehler!");
        } else {
            /*Kategorie holen*/
            KategorieSteuerung kategoriesteuerung = new KategorieSteuerung();
            KategorieGrenz kg = kategoriesteuerung.kategorieFind(hiddenValueForKategorie);
            /*Kategorie holen*/
            framekategorieedit.setKategoriegrenz(kg);
            framekategorieedit = new FrameKategorieEdit();
            framekategorieedit.setTitle("Kategorie bearbeiten");
            framekategorieedit.setVisible(true);
        }
    }//GEN-LAST:event_kategorieEditActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrameKategorieManager.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrameKategorieManager.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrameKategorieManager.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrameKategorieManager.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>


        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrameKategorieManager().setVisible(true);

            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton close;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private static javax.swing.JLabel jLabel3;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTree jTree1;
    private javax.swing.JButton kategorieCreate;
    private javax.swing.JButton kategorieDelete;
    private javax.swing.JButton kategorieEdit;
    // End of variables declaration//GEN-END:variables
}
