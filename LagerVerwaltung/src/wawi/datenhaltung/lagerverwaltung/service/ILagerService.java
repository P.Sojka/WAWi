/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.datenhaltung.lagerverwaltung.service;

import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import wawi.datenhaltung.wawidbmodel.entities.Einlieferung;
import wawi.datenhaltung.wawidbmodel.entities.Lagerort;
import wawi.datenhaltung.wawidbmodel.entities.Lagerverkehr;

/**
 *
 * @author wawi-admin
 */
public interface ILagerService {
    /********* WAWI ADMIN
     * @param em ***************/
    public void setEntityManager(EntityManager em);
    public List<Einlieferung> getAlleEinlieferungen();
    public List<Einlieferung> getEinlieferungByDatum(Date von, Date bis);
    public Einlieferung getEinlieferungById(int elfid);
    public List<Lagerverkehr> getLagerverkehrByDatum(Date von, Date bis);
    public List<Lagerverkehr> getGesamtLagerverkehr();
    

    /********* WAWI ADMIN ***************/
    public List<Lagerort> getLagerortListe();
    public Lagerort getLagerortById(int lgortid);
}
