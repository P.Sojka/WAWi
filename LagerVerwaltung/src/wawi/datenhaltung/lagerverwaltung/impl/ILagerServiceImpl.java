package wawi.datenhaltung.lagerverwaltung.impl;

import com.sun.javafx.scene.control.skin.VirtualFlow;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.EntityManager;
import wawi.datenhaltung.lagerverwaltung.service.ILagerService;
import wawi.datenhaltung.wawidbmodel.entities.Einlieferung;
import wawi.datenhaltung.wawidbmodel.entities.Lagerort;
import wawi.datenhaltung.wawidbmodel.entities.Lagerverkehr;

/**
 *
 * @author anton
 */
public class ILagerServiceImpl implements ILagerService {

    private EntityManager em;

    /**
     * Hier wird die Methode setEntityManager ausprogrammiert.Kein Rückgabewert.
     *
     * @param em
     */
    @Override
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    @Override
    public List<Einlieferung> getAlleEinlieferungen() {
        List<Einlieferung> ll = new VirtualFlow.ArrayLinkedList<>();
        ll = em.createNamedQuery("Einlieferung.findAll", Einlieferung.class).getResultList();
        return ll;
    }

    @Override
    public List<Einlieferung> getEinlieferungByDatum(Date von, Date bis) {
        List<Einlieferung> ll;
        
        ll = em.createNativeQuery("SELECT * FROM einlieferung WHERE created >='"+von+"' AND created <= '"+bis+"'",Einlieferung.class).getResultList();
        
        return ll;
    }

    @Override
    public Einlieferung getEinlieferungById(int elfid) {
        Einlieferung el = new Einlieferung();
        el = em.find(Einlieferung.class, elfid);
        return el;
    }

    @Override
    public List<Lagerverkehr> getLagerverkehrByDatum(Date von, Date bis) {
        List<Lagerverkehr> lvlist = new ArrayList<>();
        lvlist = em.createNativeQuery("SELECT * FROM lagerverkehr WHERE created >='"+von+"' AND created <= '"+bis+"'",Lagerverkehr.class).getResultList();
        return lvlist;
    }

    @Override
    public List<Lagerverkehr> getGesamtLagerverkehr() {
        List<Lagerverkehr> lvlist = new ArrayList<>();
        lvlist = em.createNamedQuery("Lagerverkehr.findAll").getResultList();
        return lvlist;
    }

    @Override
    public List<Lagerort> getLagerortListe() {
        List<Lagerort> lalist = new ArrayList<>();
        lalist = em.createNamedQuery("Lagerort.findAll").getResultList();
        return lalist;
    }

    @Override
    public Lagerort getLagerortById(int lgortid) {
        Lagerort lagerort = new Lagerort();
        lagerort = em.find(Lagerort.class, lgortid);
        return lagerort;
    }



}
