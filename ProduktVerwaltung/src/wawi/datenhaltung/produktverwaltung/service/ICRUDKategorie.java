/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.datenhaltung.produktverwaltung.service;

import java.util.List;
import javax.persistence.EntityManager;
import wawi.datenhaltung.wawidbmodel.entities.Kategorie;

/**
 *
 * @author nikita gromow
 */
public interface ICRUDKategorie {
    public void setEntityManager(EntityManager em);
    public Kategorie getKategorieById(int katid);
    public List<Kategorie> getKategorieListe();
    
    /*  @author anton li */
    public boolean insertKategorie (Kategorie k);
    public boolean updateKategorie (Kategorie k);
    public boolean deleteKategorie (int katid);
    

}
