
package wawi.datenhaltung.produktverwaltung.service;

import java.util.List;
import javax.persistence.EntityManager;
import wawi.datenhaltung.wawidbmodel.entities.Produkt;

/**
 *
 * @author nikita gromow
 */
public interface ILieferProdukt {
    public void setEntityManager(EntityManager em);
    public boolean produktStueckzahlErhoehen(int prodid,int stueckzahl);
    /******************* rakan harb ******************/
    public Produkt getProduktById(int prodid);
    public List<Produkt> getProduktListe();
    
    
    /* @authot anton li  */
    public boolean produktStueckzahlVerringern(int prodid,int stueckzahl);
      

}
