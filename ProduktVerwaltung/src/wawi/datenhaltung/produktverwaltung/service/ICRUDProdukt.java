
package wawi.datenhaltung.produktverwaltung.service;

import java.util.List;
import javax.persistence.EntityManager;
import wawi.datenhaltung.wawidbmodel.entities.Produkt;

/**
 *
 * @author nikita gromow
 */
public interface ICRUDProdukt {
    public void setEntityManager(EntityManager em);
    public boolean insertProdukt(Produkt p);
    public boolean updateProdukt(Produkt p);
    public boolean deleteProdukt(int prodid);
    /***** Patrick Sojka ********/
    public Produkt getProduktById(int prodid);
    public List<Produkt> getProduktListe();
    public List<Produkt> getAusverkaufteProdukte();
}
