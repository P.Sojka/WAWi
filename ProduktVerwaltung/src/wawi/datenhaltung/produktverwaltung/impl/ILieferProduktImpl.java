package wawi.datenhaltung.produktverwaltung.impl;

import java.util.List;
import javax.persistence.EntityManager;
import wawi.datenhaltung.produktverwaltung.service.ILieferProdukt;
import wawi.datenhaltung.wawidbmodel.entities.Produkt;

/**
 *
 * @author nikita gromow
 */
public class ILieferProduktImpl implements ILieferProdukt {

    private EntityManager em;
    private Produkt p;

    /**
     * Hier wird die Methode produktStueckZahlErhoehen ausprogrammiert.Erhöht wird durch Addition der beiden Werte.
     * @param prodid
     * @param stueckzahl
     * @return ret
     */
    @Override
    public boolean produktStueckzahlErhoehen(int prodid, int stueckzahl) {
        boolean ret = false;
        Produkt p = em.find(Produkt.class, prodid);
        if (p != null) {
            if (stueckzahl > 0) {
                p.setStueckzahl(p.getStueckzahl()+stueckzahl);
                em.merge(p);
                //em.flush();// ich brauche keine neue ID zurückzuholen, denn nichts inserted wird
                ret = true;
            }
        }
        return ret;
    }

  
    
    
    /**
     * Hier wird die Methode setEntityManager ausprogrammiert.Kein Rückgabewert.
     * @param em 
     */
    @Override
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    /**
     * Hier wird die Methode getProduktById ausprogrammiert.Liefert ein einzelnes Produkt zurück.
     * @param prodid
     * @return 
     */
    @Override
    public Produkt getProduktById(int prodid) {
        Produkt p = null;
        if(prodid>0){
            p = em.find(Produkt.class, prodid);
        }
        return p;
    }

    /**
     * Hier wird die Methode getProduktListe ausprogrammiert.Liefert eine komplette Liste zurück.
     * @return plist
     */
    @Override
    public List<Produkt> getProduktListe() {
        List<Produkt> plist;
        plist = em.createNamedQuery("Produkt.findAll").getResultList();
        return plist;
    }

    
    
    
      /* @author Anton Li */
    
    
    
    
    
    
  
    @Override
    public boolean produktStueckzahlVerringern(int prodid, int stueckzahl) {
       
         boolean ret = false;                       
        Produkt p = em.find(Produkt.class, prodid); 
        if (p != null) {
            if (stueckzahl > 0) {                    
                p.setStueckzahl(p.getStueckzahl()-stueckzahl);
                em.merge(p);             
                ret = true;
            }
        }
        return ret;
    }
    
    
  /* anton li ende */
}
