package wawi.datenhaltung.produktverwaltung.impl;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.EntityManager;
import wawi.datenhaltung.produktverwaltung.service.ICRUDProdukt;
import wawi.datenhaltung.wawidbmodel.entities.Produkt;
import java.util.*;
import javax.persistence.Query;

/**
 *
 * @author nikita gromow
 */
public class ICRUDProduktImpl implements ICRUDProdukt {

    private EntityManager em;
    private Produkt p;

    List<Produkt> produktlist;

    /**
     * Hier wird die Methode setEntityManager ausprogrammiert.Kein Rückgabewert.
     * @param em 
     */
    @Override
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    /**
     * Hier wird die Methode insertProdukt ausprogrammiert.Ein Produkt wird agelegt und liefert ihn wieder zurück.
     * @param p
     * @return ret
     */
    @Override
    public boolean insertProdukt(Produkt p) {
        boolean ret = false;
        if (p != null) {
            if (p.getProdid() == null) {
                em.persist(p);
                ret = true;
            }
        }
        return ret;
    }

    /**
     * Hier wird die Methode updateProdukt ausprogrammiert.Ein Produkt erneuert.
     * @param p
     * @return ret
     */
    @Override
    public boolean updateProdukt(Produkt p) {
        boolean ret = false;
        if (p != null) {
            if (p.getProdid() != null) {
                em.merge(p);
                ret = true;
            }
        }
        return ret;
    }

    /**
     * Hier wird die Methode deleteProdukt ausprogrammiert.Ein Produkt wird gelöscht.
     * @param prodid
     * @return ret
     */
    @Override
    public boolean deleteProdukt(int prodid) {
        boolean ret = false;
        Produkt p = em.find(Produkt.class, prodid);
        if (p != null) {
            em.remove(p);
            ret = true;
        }
        return ret;
    }

    /**
     *
     * @author patrick sojka
     */
    
    /**
     * Hier wird die Methode getProduktById ausprogrammiert.Ein Produkt wird nach seiner IDNummer gesucht und zurückgeliefert.
     * @param prodid
     * @return p
     */
    @Override
    public Produkt getProduktById(int prodid) {
        Produkt p = null;
        if (prodid > 0) {
            p = em.find(Produkt.class, prodid);
        }
        return p;
    }

    /**
     * Hier wird die Methode getProduktListe ausprogrammiert.Eine gesamte Produktliste wird zurückgeliefert.
     * @return plist
     */
    @Override
    public List<Produkt> getProduktListe() {
        List<Produkt> plist;
        plist = em.createNamedQuery("Produkt.findAll").getResultList();
        return plist;
    }

    /**
     * Hier wird die Methode getAusverkaufteProdukte ausprogrammiert.Es wird nach ausverkauften Produkten gesucht und diese werden zurückgeliefert.
     * @return plist
     */
    @Override
    public List<Produkt> getAusverkaufteProdukte() {
        List<Produkt> plist;
        plist = em.createNamedQuery("Produkt.findByStueckzahl").setParameter("stueckzahl", 0).getResultList();
        return plist;
    }
    
}
