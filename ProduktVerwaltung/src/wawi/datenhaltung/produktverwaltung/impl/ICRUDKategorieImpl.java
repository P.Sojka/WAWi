/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.datenhaltung.produktverwaltung.impl;

import java.util.List;
import javax.persistence.EntityManager;
import wawi.datenhaltung.produktverwaltung.service.ICRUDKategorie;
import wawi.datenhaltung.wawidbmodel.entities.Kategorie;
import wawi.datenhaltung.wawidbmodel.entities.Produkt;

/**
 *
 * @author rakan harb
 */
public class ICRUDKategorieImpl implements ICRUDKategorie{

    private EntityManager em;
    private Kategorie k;
    
    /**
     * Hier wird die Methode setEntityManager ausprogrammiert.Kein Rückgabewert.
     * @param em 
     */
    @Override
    public void setEntityManager(EntityManager em) {
        this.em = em;
    }
    
    /**
     * Hier wird die Methode getKategorieById ausprogrammiert.Eine Kategorie wird nach IDNummer gesucht und zurückgeliefert.
     * @param katid
     * @return k
     */
    @Override
    public Kategorie getKategorieById(int katid) {
        Kategorie kate = null;
        if(katid>0){
            kate = em.find(Kategorie.class, katid);
        }
        return kate;
    }

    /**
     * Hier wird die Methode getKategorieListe ausprogrammiert.Ein gesamte Kategorieliste wird zurückgeliefert.
     * @return klist
     */
    @Override
    public List<Kategorie> getKategorieListe() {
        List<Kategorie> katelist;
        katelist = em.createNamedQuery("Kategorie.findAll").getResultList();
        return katelist;
    }
    

                /* @author Anton */
    @Override
    public boolean insertKategorie(Kategorie k) {

        boolean ret = false;

        if (k != null) {
            if (k.getKatid() == null) { 

                em.persist(k);
                ret = true;
            }

        }

        return ret;
    }

    @Override
    public boolean updateKategorie(Kategorie k) {       

        boolean ret = false;

        if (k != null) {
            if (k.getKatid() != null) {           
            
                
            em.merge(k);
            ret = true;
            }

        }
        return ret;

    }

    @Override
    public boolean deleteKategorie(int katid) {

        boolean ret = false;
        Kategorie k = em.find(Kategorie.class, katid);
        if (k != null) {
            em.remove(k);
            ret = true;
        }
        return ret;



}

}