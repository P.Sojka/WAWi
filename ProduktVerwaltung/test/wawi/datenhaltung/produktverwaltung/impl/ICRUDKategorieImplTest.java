/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.datenhaltung.produktverwaltung.impl;

import java.util.List;
import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import wawi.datenhaltung.wawidbmodel.entities.Kategorie;
import wawi.datenhaltung.wawidbmodel.impl.IDatabaseImpl;

/**
 *
 * @author rakan harb
 */
public class ICRUDKategorieImplTest {

    private EntityManager em;
    private ICRUDKategorieImpl classUnderTest;
    private IDatabaseImpl IDataImpl;

    public ICRUDKategorieImplTest() {
    }

    /**
     * ********************* METHODEN ************************
     */
    /**
     * Löschung der DB
     *
     * @return true
     */
    private boolean deleteDatabase() {
        em.createNativeQuery("DELETE FROM bestellungsposition").executeUpdate();
        em.createNativeQuery("DELETE FROM lieferposition").executeUpdate();
        em.createNativeQuery("DELETE FROM produkt").executeUpdate();
        //Nur so kann eine Kategorie gelöscht werden, da sie als FKEY in der Produkt Tabelle eingetragen ist.
        em.createNativeQuery("DELETE FROM kategorie").executeUpdate();
        return true;
    }

    /**
     * Anlegen einer Testkategorie in die DB
     *
     * @return k
     */
    private Kategorie insertTestKategorie() {
        Kategorie k = new Kategorie(null, 1, "Arzneimittel", "Nur zur Injektion");
        em.persist(k);
        em.flush();
        return k;
    }

    /**
     * ******************* ENDE METHODEN *********************
     */
    /**
     * ANGENOMMEN der EntityManager wird korrekt geholt, UND die Implementierung
     * der ICRUDKategorie Schnittstelle wird als classUnderTest instanziiert,
     * UND der EntityManager wird per setEntityManager Methode der
     * classUnderTest gesetzt, UND die Transaktion von em wird gestartet, UND
     * die Daten der betreffenden Entitäten wurden in der DB gelöscht.
     */
    @Before
    public void angenommen() {
        IDataImpl = new IDatabaseImpl();
        //IDataImpl.useProdPU();
        IDataImpl.useDevPU();                // Dev Datenbank verwenden 
        em = IDataImpl.getEntityManager();   // das veränderte EntityManager rausholen
        classUnderTest = new ICRUDKategorieImpl();
        classUnderTest.setEntityManager(em);
        em.getTransaction().begin();
        this.deleteDatabase();
    }

    /**
     * *************************************************************************************
     */
    /**
     * *********************************** rakan harb
     * ****************************************
     */
    /**
     * *************************************************************************************
     */
    /**
     * WENN eine Testkategorie bereits in der DB existiert, UND die Methode
     * getKategorieById mit der Id der Testkategorie aufgerufen wird, DANN
     * sollte sie die Testkategorie zurückliefern.
     */
    @Test
    public void getKategorieById_00() {
        Kategorie k = this.insertTestKategorie();
        k = classUnderTest.getKategorieById(k.getKatid());
        assertNotNull(k);
    }

    /**
     * WENN eine Testkategorie nicht in der DB existiert, UND die Methode
     * getKategorieById mit der Id der Testkategorie aufgerufen wird, DANN
     * sollte sie NULL zurückliefern.
     */
    @Test
    public void getKategorieById_01() {
        //Diese TestID existiert nicht, da die DB bereits bereinigt wurde
        Kategorie k = classUnderTest.getKategorieById(777);
        assertNull(k);
    }

    /**
     * WENN x (x>0) Kategorien in der DB existieren, UND die Methode
     * getKategorieListe aufgerufen wird, DANN sollte sie eine Liste mit x
     * Kategorien zurückliefern.
     */
    @Test
    public void getKategorieListe_00() {
        this.insertTestKategorie();
        this.insertTestKategorie();
        this.insertTestKategorie();
        List<Kategorie> list = classUnderTest.getKategorieListe();
        assertNotSame(list.size(), 0);
    }

    /**
     * WENN keine Kategorien in der DB existieren, UND die Methode
     * getKategorieListe aufgerufen wird, DANN sollte sie eine leere Liste
     * zurückliefern.
     */
    @Test
    public void getKategorieListe_01() {
        List<Kategorie> list = classUnderTest.getKategorieListe();
        assertSame(list.size(), 0);
    }

    /**
     * ********************************************************************************
     */
    /**
     * ********************************************************************************
     */
    /**
     * ********************************************************************************
     */
    /**
     * AM ENDE wird die Transaktion zurück gesetzt.
     */
    @After
    public void amEnde() {
        em.getTransaction().rollback();
    }

    /* @author Anton */
 /*
     @Test: insertKategorie_00()
    WENN die Methode insertKategorie mit einer Testkategorie aufgerufen wird,
    UND die ID der Testkategorie gleich null ist,
    DANN sollte sie TRUE zurückliefern,
    UND die Testkategorie sollte in der DB existieren.
     */
    @Test
    public void insertKategorie_00() {

        Kategorie k = new Kategorie(null, 1, "Test", "Mest");
        em.persist(k);

        boolean istWert = classUnderTest.insertKategorie(k);
        em.flush();
        assertTrue(istWert);
        assertNotNull(em.find(Kategorie.class, k.getKatid()));
    }

    /*
    @Test: insertKategorie_01()
    WENN die Methode insertKategorie mit einer Testkategorie aufgerufen wird,
    UND die ID der Testkategorie ungleich null ist,
    DANN sollte sie FALSE zurückliefern,
    UND die DB wurde nicht verändert.
     */
    /**
     * Alternativ kann man auch eine Kategorie anlegen und direkt löschen und dabei die ID sich merken. 
     * Danach eine neue Kategorie erstellen mit k.getKatid als ID-nummer setzen. Es wird aber trotzdem in der Implementierung abgefangen.
     */
    @Test
    public void insertKategorie_01() {
        Kategorie k = new Kategorie(23, 1, "Test", "Mest");  
        em.persist(k);

        boolean istWert = classUnderTest.insertKategorie(k);
        em.flush();
        assertFalse(istWert);
        assertNotNull(em.find(Kategorie.class, k.getKatid()));

    }

    /*
    @Test: updateKategorie_00()
    WENN eine Testkategorie in der DB existiert,
    UND die Methode updateKategorie mit einer veränderten Testkategorie 
    (aber gleicher ID) aufgerufen wird,
    DANN sollte sie TRUE zurückliefern,
    UND die Testkategorie sollte in der DB verändert sein.
     */
    @Test
    public void updateKategorie_00() {

        Kategorie k = this.insertTestKategorie();
        String kname = k.getName();
        k.setName("Verband");
        boolean istWert = classUnderTest.updateKategorie(k);
        assertTrue(istWert);
        k = em.find(Kategorie.class, k.getKatid()); // Kategorie zurückholen
        assertNotEquals(kname, k.getName());       // Die Namen vergleichen

    }

    /*
    @Test: updateKategorie_01()
    WENN eine Testkategorie nicht in der DB existiert,
    UND die Methode updateKategorie mit der Testkategorie aufgerufen wird,
    DANN sollte sie FALSE zurückliefern,
    UND die Testkategorie sollte nicht in der DB existieren.
     */
    @Test
    public void updateKategorie_01() {

        Kategorie k = this.insertTestKategorie();
        int katid = k.getKatid();
        classUnderTest.deleteKategorie(katid);
        k = em.find(Kategorie.class, katid);
        boolean istWert = classUnderTest.updateKategorie(k);
        assertFalse(istWert);
        assertNull(em.find(Kategorie.class, katid));

    }

    /*
    @Test: deleteKategorie_00()
    WENN eine Testkategorie in der DB existiert,
    UND die Methode deleteKategorie mit der ID der Testkategorie aufgerufen wird,
    DANN sollte sie TRUE zurückliefern,
    UND die Testkategorie sollte nicht mehr in der DB existieren.
     */
    @Test
    public void deleteKategorie_00() {
        Kategorie k1 = this.insertTestKategorie();
        boolean istWert = classUnderTest.deleteKategorie(k1.getKatid());
        assertTrue(istWert);
        Kategorie k2 = em.find(Kategorie.class, k1.getKatid());
        assertNotSame(k2, k1);

    }

    /*
    @Test: deleteKategorie_01()
    WENN eine Testkategorie nicht in der DB existiert,
    UND die Methode deleteKategorie mit ID der Testkategorie aufgerufen wird,
    DANN sollte sie FALSE zurückliefern.
     */
    @Test
    public void deleteKategorie_01() {

        Kategorie k = this.insertTestKategorie();
        classUnderTest.deleteKategorie(k.getKatid());
        boolean istWert = classUnderTest.deleteKategorie(k.getKatid());
        assertFalse(istWert);

    }

}
