package wawi.datenhaltung.produktverwaltung.impl;

import java.math.BigDecimal;
import java.util.List;
import wawi.datenhaltung.produktverwaltung.impl.ILieferProduktImpl;
import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import wawi.datenhaltung.wawidbmodel.entities.Kategorie;
import wawi.datenhaltung.wawidbmodel.entities.Lager;
import wawi.datenhaltung.wawidbmodel.entities.Lagerort;
import wawi.datenhaltung.wawidbmodel.entities.Produkt;
import wawi.datenhaltung.wawidbmodel.impl.IDatabaseImpl;
import wawi.datenhaltung.wawidbmodel.service.EntityManagerSingleton;

/**
 *
 * @author nikita gromow
 */
public class ILieferProduktImplTest {

    private static EntityManagerSingleton ems;
    private static EntityManager em;
    private ILieferProduktImpl classUnderTest;
    private static IDatabaseImpl IDataImpl;

    public ILieferProduktImplTest() {
    }

    /**
     * ********************* METHODEN ************************
     */
    /**
     * Anlegen eines Testproduktes in der DB
     *
     * @return p
     */
    private Produkt insertTestProdukt() {
        BigDecimal bd = new BigDecimal(120.00);
        java.util.Date dt = new java.util.Date();

        //Testobjekt Anlegen anlegen
        Kategorie k = new Kategorie(null, 1, "KAtegorie1", "Beschreibung1");
        em.persist(k);
        em.flush();

        //Testobjekt Lager anlegen
        Lager la = new Lager(null, "Lager1", "Adresse1");
        em.persist(la);
        em.flush();

        //Testobjekt Lagerort anlegen
        Lagerort lagerort = new Lagerort();
        lagerort.setLgortid(null);
        lagerort.setLager(la);
        lagerort.setBezeichnung("Bezeichnung1");
        lagerort.setKapazitaet(1000);
        em.persist(lagerort);
        em.flush();

        //Testobjekt Produkt anlegen
        Produkt p = new Produkt();
        p.setProdid(null);
        p.setName("Stuhl");
        p.setBeschreibung("Nolzpalette");
        p.setAngelegt(dt);
        p.setStueckzahl(100);
        p.setNettopreis(bd);
        p.setMwstsatz(19);
        p.setAktiv(true);
        p.setKategorie(k);
        p.setLagerort(lagerort);
        em.persist(p);
        em.flush();
        p = em.find(Produkt.class, p.getProdid());

        return p;
    }

    /**
     * Löschung der nötigen Tabellen der DB
     *
     * @return true
     */
    private boolean deleteDatabase() {
        //Lösche alles aus der DB bevor man anfängt zu arbeiten
        em.createNativeQuery("DELETE FROM bestellungsposition").executeUpdate();
        em.createNativeQuery("DELETE FROM lieferposition").executeUpdate();
        em.createNativeQuery("DELETE FROM produkt").executeUpdate();
        return true;
    }

    /**
     * ******************* ENDE METHODEN *********************
     */
    /**
     * To solve the second issue, we must guarantee that each test fixture gets
     * a fresh entity manager. JUnit 4+ executes whatever public static method
     * annotated with @BeforeClass before firing up the class constructor.
     * Similarly, methods annotated with @AfterClass are executed after all the
     * tests and is therefore a good place for cleanup.
     */
    @BeforeClass
    public static void testOpenSetEntityManager() {
        ems = EntityManagerSingleton.getInstance();
    }

    /**
     * ANGENOMMEN der EntityManager wird korrekt geholt, UND die Implementierung
     * der ILieferProdukt Schnittstelle wird als classUnderTest instanziiert,
     * UND der EntityManager wird per setEntityManager Methode der
     * classUnderTest gesetzt, UND die Transaktion von em wird gestartet, UND
     * die Daten der betreffenden Entitäten wurden in der DB gelöscht.
     */
    @Before
    public void angenommen() {
        IDataImpl = new IDatabaseImpl();
        //IDataImpl.useProdPU();
        IDataImpl.useDevPU();
        //em = ems.getEntityManager(); // Nur wenn ich es mit @BeforeClass und @AfterClass ausführen möchte
        em = IDataImpl.getEntityManager();
        classUnderTest = new ILieferProduktImpl();
        classUnderTest.setEntityManager(em);
        em.getTransaction().begin();
        this.deleteDatabase();
    }

    /**
     * WENN ein Testprodukt bereits in der DB existiert, UND die Methode
     * getProduktStueckzahlErhoehen mit der Id des Testprodukts und einer
     * Stückzahl x > 0 aufgerufen wird, DANN sollte sie TRUE zurückliefern, UND
     * die Stückzahl des Testprodukts sollte in der DB um x erhöht sein.
     */
    @Test
    public void produktStueckzahlErhoehen_00() {
        Produkt p = this.insertTestProdukt();
        int stueckZahlAlt = p.getStueckzahl();//soll 100 sein      
        boolean istWert = classUnderTest.produktStueckzahlErhoehen(p.getProdid(), 22);
        assertTrue(istWert);
        p = em.find(Produkt.class, p.getProdid());
        assertNotEquals(stueckZahlAlt, p.getStueckzahl());// soll 122 sein
   
    }

    /**
     * WENN ein Testprodukt bereits in der DB existiert, UND die Methode
     * getProduktStueckzahlErhoehen mit der Id des Testprodukts und einer
     * Stückzahl x kleiner= 0 aufgerufen wird, DANN sollte sie FALSE
     * zurückliefern, UND die Stückzahl des Testprodukts sollte in der DB
     * unverändert sein.
     */
    @Test
    public void produktStueckzahlErhoehen_01() {
        Produkt p = this.insertTestProdukt();
        int stueckZahlAlt = p.getStueckzahl();//soll 100 sein      
        boolean istWert = classUnderTest.produktStueckzahlErhoehen(p.getProdid(), -22);
        assertFalse(istWert);
        p = em.find(Produkt.class, p.getProdid());
        assertEquals(stueckZahlAlt, p.getStueckzahl());// soll 100 sein
    }

    /**
     * WENN ein Testprodukt nicht in der DB existiert, UND die Methode
     * getProduktStueckzahlErhoehen mit der Id des Testprodukts und einer
     * Stückzahl x > 0 aufgerufen wird, DANN sollte sie FALSE zurückliefern, UND
     * die Stückzahl des Testprodukts weiterhin nicht in der DB existieren.
     *
     */
    @Test
    public void produktStueckzahlErhoehen_02() {
        //Die DB ist zu der Zeit leer, also existiert kein Produkt
        //Alternativ: Produkt p = this.insertTestProdukt();int pid = p.getProdid();this.deleteDatabase();
        boolean istWert = classUnderTest.produktStueckzahlErhoehen(999, 22);
        assertFalse(istWert);
    }

    /**
     * *************************************************************************************
     */
    /**
     * *********************************** rakan harb ****************************************
     */
    /**
     * *************************************************************************************
     */
    /**
     * WENN ein Testprodukt bereits in der DB existiert, UND die Methode
     * getProduktById mit der Id des Testprodukts aufgerufen wird, DANN sollte
     * sie das Testprodukt zurückliefern.
     */
    @Test
    public void getProduktById_00() {
        Produkt p = this.insertTestProdukt();
        p = classUnderTest.getProduktById(p.getProdid());
        assertNotNull(p);
    }

    /**
     * WENN ein Testprodukt nicht in der DB existiert, UND die Methode
     * getProduktById mit der Id des Testprodukts aufgerufen wird, DANN sollte
     * sie NULL zurückliefern.
     */
    @Test
    public void getProduktById_01() {
        Produkt p = classUnderTest.getProduktById(777);
        assertNull(p);
    }

    /**
     * WENN x (x>0) Produkte in der DB existieren, UND die Methode
     * getProduktListe aufgerufen wird, DANN sollte sie eine Liste mit x
     * Produkten zurückliefern.
     */
    @Test
    public void getProduktListe_00() {
        this.insertTestProdukt();
        this.insertTestProdukt();
        this.insertTestProdukt();
        List<Produkt> list = classUnderTest.getProduktListe();
        assertNotEquals(list.size(), 0);
    }

    /**
     * WENN keine Produkte in der DB existieren, UND die Methode getProduktListe
     * aufgerufen wird, DANN sollte sie eine leere Liste zurückliefern.
     */
    @Test
    public void getProduktListe_01() {
        List<Produkt> list = classUnderTest.getProduktListe();
        assertEquals(list.size(), 0);
    }

    /**
     * Rollback.AM ENDE wird die Transaktion zurück gesetzt.
     */
    @After
    public void amEnde() {
        em.getTransaction().rollback();
        //em.getTransaction().commit();
    }

    /**
     * Test of testCloseSetEntityManager methodto close existing em.
     */
    @AfterClass
    public static void testCloseSetEntityManager() {
        em.close();
    }

}
