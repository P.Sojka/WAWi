package wawi.datenhaltung.produktverwaltung.impl;

import wawi.datenhaltung.produktverwaltung.impl.ICRUDProduktImpl;
import java.math.BigDecimal;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.BeforeClass;
import wawi.datenhaltung.wawidbmodel.entities.Kategorie;
import wawi.datenhaltung.wawidbmodel.entities.Lager;
import wawi.datenhaltung.wawidbmodel.entities.Lagerort;
import wawi.datenhaltung.wawidbmodel.entities.Produkt;
import wawi.datenhaltung.wawidbmodel.impl.IDatabaseImpl;
import wawi.datenhaltung.wawidbmodel.service.EntityManagerSingleton;

/**
 *
 * @author nikita gromow
 */
public class ICRUDProduktImplTest {

    private static EntityManagerSingleton ems;
    private static EntityManager em;
    private ICRUDProduktImpl classUnderTest;
    private static IDatabaseImpl IDataImpl;

    public ICRUDProduktImplTest() {
    }

    /**
     * ********************* METHODEN ************************
     */
    /**
     * To solve the second issue, we must guarantee that each test fixture gets
     * a fresh entity manager. JUnit 4+ executes whatever public static method
     * annotated with @BeforeClass before firing up the class constructor.
     * Similarly, methods annotated with @AfterClass are executed after all the
     * tests and is therefore a good place for cleanup.
     */
    @BeforeClass
    public static void testOpenSetEntityManager() {
        ems = EntityManagerSingleton.getInstance();
    }

    /**
     * Anlegen eines Testproduktes in der DB
     *
     * @return p
     */
    private Produkt insertTestProdukt() {
        BigDecimal bd = new BigDecimal(120.00);
        java.util.Date dt = new java.util.Date();

        //Testobjekt Anlegen anlegen
        Kategorie k = new Kategorie(null, 1, "KAtegorie1", "Beschreibung1");
        em.persist(k);
        em.flush();

        //Testobjekt Lager anlegen
        Lager la = new Lager(null, "Lager1", "Adresse1");
        em.persist(la);
        em.flush();

        //Testobjekt Lagerort anlegen
        Lagerort lagerort = new Lagerort();
        lagerort.setLgortid(null);
        lagerort.setLager(la);
        lagerort.setBezeichnung("Bezeichnung1");
        lagerort.setKapazitaet(1000);
        em.persist(lagerort);
        em.flush();

        //Testobjekt Produkt anlegen
        Produkt p = new Produkt();
        p.setProdid(null);
        p.setName("Stuhl");
        p.setBeschreibung("Nolzpalette");
        p.setAngelegt(dt);
        p.setStueckzahl(100);
        p.setNettopreis(bd);
        p.setMwstsatz(19);
        p.setAktiv(true);
        p.setKategorie(k);
        p.setLagerort(lagerort);
        em.persist(p);
        em.flush();
        p = em.find(Produkt.class, p.getProdid());

        return p;
    }

    /**
     * Löschung der nötigen Tabellen der DB
     *
     * @return true
     */
    private boolean deleteDatabase() {
        //Lösche alles aus der DB bevor man anfängt zu arbeiten
        em.createNativeQuery("DELETE FROM bestellungsposition").executeUpdate();
        em.createNativeQuery("DELETE FROM lieferposition").executeUpdate();
        em.createNativeQuery("DELETE FROM produkt").executeUpdate();
        return true;
    }

    /**
     * ******************* ENDE METHODEN *********************
     */
    /**
     * ANGENOMMEN der EntityManager wird korrekt geholt, UND die Implementierung
     * der ICRUDProdukt Schnittstelle wird als classUnderTest instanziiert, UND
     * der EntityManager wird per setEntityManager Methode der classUnderTest
     * gesetzt, UND die Transaktion von em wird gestartet, UND die Daten der
     * betreffenden Entitäten wurden in der DB gelöscht.
     */
    @Before
    public void angenommen() {
        IDataImpl = new IDatabaseImpl();
        //IDataImpl.useProdPU();
        IDataImpl.useDevPU();
        //em = ems.getEntityManager(); // Nur wenn ich es mit @BeforeClass und @AfterClass ausführen möchte
        em = IDataImpl.getEntityManager();
        classUnderTest = new ICRUDProduktImpl();
        classUnderTest.setEntityManager(em);
        em.getTransaction().begin();
        this.deleteDatabase();
    }

    /**
     * WENN die Methode insertProdukt mit einem Testprodukt aufgerufen wird, UND
     * die ID des Testprodukts gleich null ist, DANN sollte sie TRUE
     * zurückliefern, UND das Testprodukt sollte in der DB existieren.
     */
    @Test
    public void insertProdukt_00() {
        BigDecimal bd = new BigDecimal(120.00);
        java.util.Date dt = new java.util.Date();

        Kategorie k = new Kategorie(null, 1, "KAtegorie1", "Beschreibung1");
        em.persist(k);

        Lager la = new Lager(null, "Lager1", "Adresse1");
        em.persist(la);

        Lagerort lagerort = new Lagerort();
        lagerort.setLgortid(null);
        lagerort.setLager(la);
        lagerort.setBezeichnung("Bezeichnung1");
        lagerort.setKapazitaet(1000);
        em.persist(lagerort);

        Produkt p = new Produkt();
        p.setProdid(null);
        p.setName("Stuhl");
        p.setBeschreibung("Nolzpalette");
        p.setAngelegt(dt);
        p.setStueckzahl(100);
        p.setNettopreis(bd);
        p.setMwstsatz(19);
        p.setAktiv(true);
        p.setKategorie(k);
        p.setLagerort(lagerort);

        boolean istWert = classUnderTest.insertProdukt(p);
        em.flush();
        assertTrue(istWert);
        assertNotNull(em.find(Produkt.class, p.getProdid()));
    }

    /**
     * WENN die Methode insertProdukt mit einem Testprodukt aufgerufen wird, UND
     * die ID des Testprodukts ungleich null ist, DANN sollte sie FALSE
     * zurückliefern, UND die DB wurde nicht verändert.
     */
    @Test
    public void insertProdukt_01() {
        Produkt p = this.insertTestProdukt();
        int pid = p.getProdid();
        em.remove(p);
        BigDecimal bd = new BigDecimal(120.00);
        java.util.Date dt = new java.util.Date();
        Kategorie k = new Kategorie(null, 1, "KAtegorie1", "Beschreibung1");
        em.persist(k);
        Lager la = new Lager(null, "Lager1", "Adresse1");
        em.persist(la);
        Lagerort lagerort = new Lagerort();
        lagerort.setLgortid(null);
        lagerort.setLager(la);
        lagerort.setBezeichnung("Bezeichnung1");
        lagerort.setKapazitaet(1000);
        em.persist(lagerort);
        p = new Produkt();
        //Ein Produkt mit dieser ID existiert ncht mehr,da oben bereits gelöscht wurde 
        p.setProdid(pid);
        p.setName("Stuhl");
        p.setBeschreibung("Nolzpalette");
        p.setAngelegt(dt);
        p.setStueckzahl(100);
        p.setNettopreis(bd);
        p.setMwstsatz(19);
        p.setAktiv(true);
        p.setKategorie(k);
        p.setLagerort(lagerort);

        boolean istWert = classUnderTest.insertProdukt(p);
        em.flush();
        assertFalse(istWert);
        assertNull(em.find(Produkt.class, p.getProdid()));
    }

    /**
     * WENN ein Testprodukt in der DB existiert, UND die Methode updateProdukt
     * mit einem verändertem Testprodukt (aber gleicher ID) aufgerufen wird,
     * DANN sollte sie TRUE zurückliefern, UND das Testprodukt sollte in der DB
     * verändert sein.
     */
    @Test
    public void updateProdukt_00() {
        Produkt p = this.insertTestProdukt();
        String pname = p.getName();
        p.setName("IKEA Holz");
        boolean istWert = classUnderTest.updateProdukt(p);
        assertTrue(istWert);
        // Produkt noch einmal zurückholen
        p = em.find(Produkt.class, p.getProdid());
        // Vergleichen zwischen dem alten Produktname und dem neuen
        assertNotEquals(pname, p.getName());
    }

    /**
     * WENN ein Testprodukt nicht in der DB existiert, UND die Methode
     * updateProdukt mit dem Testprodukt aufgerufen wird, DANN sollte sie FALSE
     * zurückliefern, UND das Testprodukt sollte nicht in der DB existieren.
     */
    @Test
    public void updateProdukt_01() {
        Produkt p = this.insertTestProdukt();
        int pid = p.getProdid();
        classUnderTest.deleteProdukt(pid);
        p = em.find(Produkt.class, pid);
        boolean istWert = classUnderTest.updateProdukt(p);
        assertFalse(istWert);
        assertNull(em.find(Produkt.class, pid));
    }

    /**
     * WENN ein Testprodukt in der DB existiert, UND die Methode deleteProdukt
     * mit der ID des Testprodukts aufgerufen wird,DANN sollte sie TRUE
     * zurückliefern, UND das Testprodukt sollte nicht mehr in der DB
     * existieren.
     */
    @Test
    public void deleteProdukt_00() {
        Produkt p1 = this.insertTestProdukt();
        boolean istWert = classUnderTest.deleteProdukt(p1.getProdid());
        assertTrue(istWert);
        Produkt p2 = em.find(Produkt.class, p1.getProdid());
        assertNotSame(p2, p1);
    }

    /**
     * WENN ein Testprodukt nicht in der DB existiert, UND die Methode
     * deleteProdukt mit der ID des Testprodukts aufgerufen wird, DANN sollte
     * sie FALSE zurückliefern.
     */
    @Test
    public void deleteProdukt_01() {
        Produkt p = this.insertTestProdukt();
        classUnderTest.deleteProdukt(p.getProdid());
        boolean istWert = classUnderTest.deleteProdukt(p.getProdid());
        assertFalse(istWert);
    }

    /* **************************************************  PATRICK SOJKA ***********************************************/
    /** 
     * WENN ein Testprodukt bereits in der DB existiert,
     * UND die Methode getProduktById mit der Id des Testprodukts aufgerufen wird,
     * DANN sollte sie das Testprodukt zurückliefern.
     */
   
    @Test
    public void getProduktById_00() {
        Produkt p = this.insertTestProdukt();
        p = classUnderTest.getProduktById(p.getProdid());
        assertNotNull(p);
    }

    /**
     * WENN ein Testprodukt nicht in der DB existiert,
     * UND die Methode getProduktById mit der Id des Testprodukts aufgerufen wird,
     * DANN sollte sie NULL zurückliefern.
     */
    
    @Test
    public void getProduktById_01() {
        Produkt p = this.insertTestProdukt();
        classUnderTest.deleteProdukt(p.getProdid());
        p = classUnderTest.getProduktById(p.getProdid());
        assertNull(p);
    }

    /**
     * WENN x (x>0) Produkte in der DB existieren,
     * UND die Methode getProduktListe aufgerufen wird,
     * DANN sollte sie eine Liste mit x Produkten zurückliefern. 
     */
    
    @Test
    public void getProduktListe_00() {
        this.insertTestProdukt();
        this.insertTestProdukt();
        this.insertTestProdukt();
        List<Produkt> list = classUnderTest.getProduktListe();
        assertNotEquals(list.size(), 0);
    }

    /**
     * WENN keine Produkte in der DB existieren,
     * UND die Methode getProduktListe aufgerufen wird,
     * DANN sollte sie eine leere Liste zurückliefern. 
     */
    
    @Test
    public void getProduktListe_01() {
        List<Produkt> list = classUnderTest.getProduktListe();
        assertSame(list.size(), 0);
    }

    /**
     * WENN x (x>2) Produkte in der Datenbank existieren,
     * UND y (y>0 und y(kleiner)x)Produkte mit einer Stückzahl = 0 existieren,
     * UND die Methode getAusverkaufteProdukte aufgerufen wird,
     * DANN sollte sie eine Liste mit y Produkten zurückliefern. 
     */
    
    @Test
    public void getAusverkaufteProdukte_00() {
        Produkt p1 = this.insertTestProdukt();
        Produkt p2 = this.insertTestProdukt();
        Produkt p3 = this.insertTestProdukt();
        p1.setStueckzahl(0);
        p3.setStueckzahl(0);
        classUnderTest.updateProdukt(p1);
        classUnderTest.updateProdukt(p3);
        List<Produkt> list = classUnderTest.getAusverkaufteProdukte();
        assertEquals(list.size(), 2);
    }

    /** 
    * WENN x (x>0) Produkte in der Datenbank existieren,
    * UND keine Produkte mit einer Stückzahl = 0 existieren,
    * UND die Methode getAusverkaufteProdukte aufgerufen wird,
    * DANN sollte sie eine leere Liste zurückliefern. 
    */
    
    @Test
    public void getAusverkaufteProdukte_01() {
        this.insertTestProdukt();
        this.insertTestProdukt();
        this.insertTestProdukt();
        this.insertTestProdukt();
        List<Produkt> list = classUnderTest.getProduktListe();
        list = classUnderTest.getAusverkaufteProdukte();
        assertEquals(list.size(), 0);
    }

    /* **************************************************  PATRICK SOJKA ***********************************************/
    /**
     * Rollback.AM ENDE wird die Transaktion zurück gesetzt.
     */
    @After
    public void amEnde() {
        em.getTransaction().rollback();
        //em.getTransaction().commit();
    }

    /**
     * Test of testCloseSetEntityManager methodto close existing em.
     */
    @AfterClass
    public static void testCloseSetEntityManager() {
        em.close();
    }

}
