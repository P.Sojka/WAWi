-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Erstellungszeit: 03. Apr 2016 um 02:21
-- Server Version: 5.5.47-0+deb8u1
-- PHP-Version: 5.6.17-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Datenbank: `wawidb_dev`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bestellung`
--

CREATE TABLE IF NOT EXISTS `bestellung` (
`bid` int(11) NOT NULL,
  `kunde` int(11) NOT NULL,
  `lieferadresse` text COLLATE utf8_unicode_ci NOT NULL,
  `rechnungsadresse` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `status` enum('n','b','r','s','v') COLLATE utf8_unicode_ci NOT NULL,
  `gesamtnetto` decimal(12,2) NOT NULL,
  `gesamtbrutto` decimal(12,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `bestellung`
--

INSERT INTO `bestellung` (`bid`, `kunde`, `lieferadresse`, `rechnungsadresse`, `created`, `status`, `gesamtnetto`, `gesamtbrutto`) VALUES
(1, 1, 'Steingasse 9, 50667 Köln', 'Steingasse 9, 50667 Köln', '2015-09-05 14:37:11', 'n', 540.00, 642.60),
(2, 2, 'Hauptstraße 81, 50767 Köln', 'Hauptstraße 81, 50767 Köln', '2015-09-10 14:37:11', 'n', 190.00, 226.10),
(3, 1, 'Steingasse 9, 50667 Köln', 'Steingasse 9, 50667 Köln', '2015-08-10 14:37:11', 'b', 150.00, 178.50),
(4, 2, 'Hauptstraße 81, 50767 Köln', 'Hauptstraße 81, 50767 Köln', '2015-08-12 14:37:11', 'v', 0.00, 0.00),
(5, 3, 'Waldgasse 59, 51147 Köln', 'Waldgasse 59, 51147 Köln', '2015-04-10 14:37:11', 'r', 150.00, 178.50),
(6, 3, 'Waldgasse 59, 51147 Köln', 'Waldgasse 59, 51147 Köln', '2015-05-12 14:37:11', 'r', 200.00, 238.00);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `bestellungsposition`
--

CREATE TABLE IF NOT EXISTS `bestellungsposition` (
`bpid` int(11) NOT NULL,
  `bestellung` int(11) NOT NULL,
  `produkt` int(11) NOT NULL,
  `anzahl` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `bestellungsposition`
--

INSERT INTO `bestellungsposition` (`bpid`, `bestellung`, `produkt`, `anzahl`) VALUES
(1, 1, 1, 2),
(2, 1, 4, 2),
(3, 2, 2, 1),
(4, 2, 5, 1),
(5, 3, 3, 1),
(6, 3, 6, 1),
(7, 4, 3, 1),
(8, 4, 6, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `einlieferung`
--

CREATE TABLE IF NOT EXISTS `einlieferung` (
`elfid` int(11) NOT NULL,
  `lieferant` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `gesamtpreis` decimal(12,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `einlieferung`
--

INSERT INTO `einlieferung` (`elfid`, `lieferant`, `created`, `gesamtpreis`) VALUES
(1, 1, '2015-05-05 14:37:11', 4610.00),
(2, 2, '2015-05-05 14:37:11', 2910.00),
(3, 3, '2015-08-05 14:37:11', 0.00),
(4, 4, '2015-08-04 14:37:11', 0.00);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kategorie`
--

CREATE TABLE IF NOT EXISTS `kategorie` (
`katid` int(11) NOT NULL,
  `parentkatid` int(11) NOT NULL DEFAULT '1',
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `beschreibung` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `kategorie`
--

INSERT INTO `kategorie` (`katid`, `parentkatid`, `name`, `beschreibung`) VALUES
(1, 1, 'Geräte', 'Fitnessgeräte, Sportgeräte'),
(2, 1, 'Geräte', 'Fitnessgeräte, Sportgeräte'),
(3, 1, 'Kleidung', 'Sportkleidung'),
(4, 1, 'Elektronik', 'Pulsuhren, Geotracker etc.');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `kunde`
--

CREATE TABLE IF NOT EXISTS `kunde` (
`kid` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `vorname` text COLLATE utf8_unicode_ci NOT NULL,
  `adresse` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `kunde`
--

INSERT INTO `kunde` (`kid`, `name`, `vorname`, `adresse`, `created`) VALUES
(1, 'Schmitz', 'Josef', 'Steingasse 9, 50667 Köln', '2016-02-18 14:37:10'),
(2, 'Meier', 'Ursula', 'Hauptstraße 81, 50767 Köln', '2016-02-18 14:37:10'),
(3, 'Mueller', 'Peter', 'Waldgasse 59, 51147 Köln', '2016-02-18 14:37:10'),
(4, 'Becker', 'Karin', 'Turmstraße 27, 51063 Köln', '2016-02-18 14:37:10'),
(5, 'Kaufmann', 'Michael', 'Baumgasse 48,  50765 Köln', '2016-02-18 14:37:10'),
(6, 'Schmidt', 'Helga', 'Kölnerstraße 77, 50737 Köln', '2016-02-18 14:37:10'),
(7, 'Schneider', 'Thomas', 'Seeweg 5, 51147 Köln', '2016-02-18 14:37:10'),
(8, 'Fischer', 'Sabine', 'Dorfstraße 15, 50767 Köln', '2016-02-18 14:37:10'),
(9, 'Weber', 'Andreas', 'Kiefernweg 2, 50670 Köln', '2016-02-18 14:37:10'),
(10, 'Hoffmann', 'Ingrid', 'Schulstraße 55, 50670 Köln', '2016-02-18 14:37:10'),
(11, 'Mayer', 'Max', 'Auf Schalke', '2016-02-18 14:37:17');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lager`
--

CREATE TABLE IF NOT EXISTS `lager` (
`lagerid` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `addresse` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `lager`
--

INSERT INTO `lager` (`lagerid`, `name`, `addresse`) VALUES
(1, 'Lager Aachen', 'Aachen'),
(2, 'Lager Bonn', 'Bonn'),
(3, 'Lager Leverkusen', 'Leverkusen'),
(4, 'Lager Köln', 'Köln');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lagerort`
--

CREATE TABLE IF NOT EXISTS `lagerort` (
`lgortid` int(11) NOT NULL,
  `lager` int(11) NOT NULL,
  `bezeichnung` text COLLATE utf8_unicode_ci NOT NULL,
  `kapazitaet` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `lagerort`
--

INSERT INTO `lagerort` (`lgortid`, `lager`, `bezeichnung`, `kapazitaet`) VALUES
(1, 4, 'Gerätehalle K', 20),
(2, 4, 'Kleiderhalle K', 100),
(3, 4, 'Elektronikregal K', 30),
(4, 4, 'Schuhregal K', 50),
(5, 3, 'Gerätehalle Lev', 20),
(6, 3, 'Kleiderhalle Lev', 100),
(7, 3, 'Elektronikregal Lev', 30),
(8, 3, 'Schuhregal Lev', 50),
(9, 2, 'Gerätehalle Bn', 20),
(10, 2, 'Kleiderhalle Bn', 100),
(11, 2, 'Elektronikregal Bn', 30),
(12, 2, 'Schuhregal Bn', 50),
(13, 1, 'Gerätehalle Aa', 20),
(14, 1, 'Kleiderhalle Aa', 100),
(15, 1, 'Elektronikregal Aa', 30),
(16, 1, 'Schuhregal Aa', 50);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lagerverkehr`
--

CREATE TABLE IF NOT EXISTS `lagerverkehr` (
`lgvid` int(11) NOT NULL,
  `lager` int(11) NOT NULL,
  `einlieferung` int(11) DEFAULT NULL,
  `bestellung` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `status` enum('ok','del') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ok'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `lagerverkehr`
--

INSERT INTO `lagerverkehr` (`lgvid`, `lager`, `einlieferung`, `bestellung`, `created`, `status`) VALUES
(1, 1, 1, NULL, '2015-05-05 14:37:11', 'ok'),
(2, 2, 1, NULL, '2015-05-05 14:37:11', 'ok'),
(3, 4, 1, NULL, '2015-05-05 14:37:11', 'ok'),
(4, 3, 2, NULL, '2015-08-05 14:37:11', 'ok'),
(5, 1, 2, NULL, '2015-08-05 14:37:11', 'ok'),
(6, 4, 3, NULL, '2015-08-04 14:37:11', 'ok'),
(7, 1, NULL, 1, '2015-09-05 14:37:11', 'ok'),
(8, 2, NULL, 2, '2015-09-10 14:37:11', 'ok'),
(9, 1, NULL, 3, '2015-09-10 14:37:11', 'ok'),
(10, 2, NULL, 4, '2014-12-12 14:37:11', 'ok');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lieferant`
--

CREATE TABLE IF NOT EXISTS `lieferant` (
`lfid` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `vorname` text COLLATE utf8_unicode_ci NOT NULL,
  `telefon` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `email` text COLLATE utf8_unicode_ci NOT NULL,
  `firma` text COLLATE utf8_unicode_ci,
  `aktiv` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `lieferant`
--

INSERT INTO `lieferant` (`lfid`, `name`, `vorname`, `telefon`, `email`, `firma`, `aktiv`) VALUES
(1, 'Krüger', 'Bernd', '0221 / 234234', 'bernd.k@t-offline.de', NULL, 1),
(2, 'Koch', 'Maria', '0221 / 543543', 'koch.m@t-offline.de', NULL, 1),
(3, 'Richter', 'Manfred', '0221 / 456456', 'richter.m@t-offline.de', NULL, 1),
(4, 'Schulz', 'Heike', '0221 / 987987', 'schulz.h@t-offline.de', NULL, 1),
(5, 'Wagner', 'Heinz', '0221 / 123321', 'wagner.h@t-offline.de', NULL, 1),
(6, 'Zimmermann', 'Martina', '0221 / 741147', 'zimmermann.m@t-offline.de', NULL, 1);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `lieferposition`
--

CREATE TABLE IF NOT EXISTS `lieferposition` (
`lpid` int(11) NOT NULL,
  `einlieferung` int(11) NOT NULL,
  `produkt` int(11) NOT NULL,
  `anzahl` int(11) NOT NULL,
  `kaufpreis` decimal(12,2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `lieferposition`
--

INSERT INTO `lieferposition` (`lpid`, `einlieferung`, `produkt`, `anzahl`, `kaufpreis`) VALUES
(1, 1, 1, 20, 135.00),
(2, 1, 2, 10, 115.00),
(3, 1, 3, 8, 95.00),
(4, 2, 4, 20, 95.00),
(5, 2, 5, 10, 65.00),
(6, 2, 6, 8, 45.00);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `nachricht`
--

CREATE TABLE IF NOT EXISTS `nachricht` (
`nid` int(11) NOT NULL,
  `kunde` int(11) NOT NULL,
  `betreff` text COLLATE utf8_unicode_ci NOT NULL,
  `nachricht` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `status` enum('gelesen','ungelesen') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'ungelesen',
  `typ` enum('anwawi','ankunde') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'anwawi'
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `nachricht`
--

INSERT INTO `nachricht` (`nid`, `kunde`, `betreff`, `nachricht`, `created`, `status`, `typ`) VALUES
(1, 1, 'Rechnung', 'Die Rechnung für Ihre Bestellung liegt bei', '2016-02-18 14:37:11', 'gelesen', 'ankunde'),
(2, 2, 'Rechnung', 'Die Rechnung für Ihre Bestellung liegt bei', '2016-02-18 14:37:11', 'gelesen', 'ankunde'),
(3, 3, 'Rechnung', 'Die Rechnung für Ihre Bestellung liegt bei', '2016-02-18 14:37:11', 'gelesen', 'ankunde'),
(4, 1, 'Mahnung', 'Die Mahnung für Ihre Bestellung liegt bei', '2016-02-18 14:37:11', 'ungelesen', 'ankunde'),
(5, 2, 'Mahnung', 'Die Mahnung für Ihre Bestellung liegt bei', '2016-02-18 14:37:11', 'ungelesen', 'ankunde'),
(6, 3, 'Mahnung', 'Die Mahnung für Ihre Bestellung liegt bei', '2016-02-18 14:37:11', 'ungelesen', 'ankunde'),
(7, 3, 'Stornierung', 'Bitte stornieren Sie meine Bestellung', '2016-02-18 14:37:11', 'ungelesen', 'anwawi'),
(8, 2, 'Stornierung', 'Bitte stornieren Sie meine Bestellung', '2016-02-18 14:37:11', 'ungelesen', 'anwawi'),
(9, 3, 'Änderung', 'Bitte entfernen Sie Pos 3 aus meiner Bestellung', '2016-02-18 14:37:11', 'ungelesen', 'anwawi'),
(10, 2, 'Änderung', 'Bitte verdoppeln Sie die Anzahl von Pos 2', '2016-02-18 14:37:11', 'ungelesen', 'anwawi');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `produkt`
--

CREATE TABLE IF NOT EXISTS `produkt` (
`prodid` int(11) NOT NULL,
  `name` text COLLATE utf8_unicode_ci NOT NULL,
  `beschreibung` text COLLATE utf8_unicode_ci NOT NULL,
  `angelegt` datetime NOT NULL,
  `stueckzahl` int(11) NOT NULL,
  `nettopreis` decimal(12,2) NOT NULL,
  `mwstsatz` int(11) NOT NULL DEFAULT '19',
  `aktiv` tinyint(1) NOT NULL,
  `kategorie` int(11) NOT NULL,
  `lagerort` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Daten für Tabelle `produkt`
--

INSERT INTO `produkt` (`prodid`, `name`, `beschreibung`, `angelegt`, `stueckzahl`, `nettopreis`, `mwstsatz`, `aktiv`, `kategorie`, `lagerort`) VALUES
(1, 'Trainingsanzug ADIDAS 2016', 'DFB N11 Trainigsanzug 2016', '2016-02-18 14:37:10', 20, 150.00, 19, 1, 3, 2),
(2, 'Trainingsanzug ADIDAS 2014', 'DFB N11 Trainigsanzug 2014', '2016-02-18 14:37:10', 10, 120.00, 19, 1, 3, 14),
(3, 'Trainingsanzug ADIDAS 2012', 'DFB N11 Trainigsanzug 2012', '2016-02-18 14:37:10', 8, 100.00, 19, 1, 3, 10),
(4, 'Fußballschuhe ADIDAS 2016', 'DFB N11 Fußballschuhe 2016', '2016-02-18 14:37:10', 20, 120.00, 19, 1, 3, 8),
(5, 'Fußballschuhe ADIDAS 2014', 'DFB N11 Fußballschuhe 2014', '2016-02-18 14:37:10', 10, 70.00, 19, 1, 3, 16),
(6, 'Fußballschuhe ADIDAS 2012', 'DFB N11 Fußballschuhe 2012', '2016-02-18 14:37:10', 8, 50.00, 19, 1, 3, 16);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `bestellung`
--
ALTER TABLE `bestellung`
 ADD PRIMARY KEY (`bid`), ADD KEY `kunde` (`kunde`);

--
-- Indizes für die Tabelle `bestellungsposition`
--
ALTER TABLE `bestellungsposition`
 ADD PRIMARY KEY (`bpid`), ADD KEY `bestellung` (`bestellung`), ADD KEY `produkt` (`produkt`);

--
-- Indizes für die Tabelle `einlieferung`
--
ALTER TABLE `einlieferung`
 ADD PRIMARY KEY (`elfid`), ADD KEY `lieferant` (`lieferant`);

--
-- Indizes für die Tabelle `kategorie`
--
ALTER TABLE `kategorie`
 ADD PRIMARY KEY (`katid`);

--
-- Indizes für die Tabelle `kunde`
--
ALTER TABLE `kunde`
 ADD PRIMARY KEY (`kid`);

--
-- Indizes für die Tabelle `lager`
--
ALTER TABLE `lager`
 ADD PRIMARY KEY (`lagerid`);

--
-- Indizes für die Tabelle `lagerort`
--
ALTER TABLE `lagerort`
 ADD PRIMARY KEY (`lgortid`), ADD KEY `lager` (`lager`);

--
-- Indizes für die Tabelle `lagerverkehr`
--
ALTER TABLE `lagerverkehr`
 ADD PRIMARY KEY (`lgvid`), ADD KEY `einlieferung` (`einlieferung`,`bestellung`), ADD KEY `bestellung` (`bestellung`), ADD KEY `lager` (`lager`);

--
-- Indizes für die Tabelle `lieferant`
--
ALTER TABLE `lieferant`
 ADD PRIMARY KEY (`lfid`);

--
-- Indizes für die Tabelle `lieferposition`
--
ALTER TABLE `lieferposition`
 ADD PRIMARY KEY (`lpid`), ADD KEY `einlieferung` (`einlieferung`,`produkt`), ADD KEY `produkt` (`produkt`);

--
-- Indizes für die Tabelle `nachricht`
--
ALTER TABLE `nachricht`
 ADD PRIMARY KEY (`nid`), ADD KEY `kunde` (`kunde`);

--
-- Indizes für die Tabelle `produkt`
--
ALTER TABLE `produkt`
 ADD PRIMARY KEY (`prodid`), ADD KEY `kategorie` (`kategorie`), ADD KEY `lagerort` (`lagerort`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `bestellung`
--
ALTER TABLE `bestellung`
MODIFY `bid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `bestellungsposition`
--
ALTER TABLE `bestellungsposition`
MODIFY `bpid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT für Tabelle `einlieferung`
--
ALTER TABLE `einlieferung`
MODIFY `elfid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `kategorie`
--
ALTER TABLE `kategorie`
MODIFY `katid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `kunde`
--
ALTER TABLE `kunde`
MODIFY `kid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT für Tabelle `lager`
--
ALTER TABLE `lager`
MODIFY `lagerid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT für Tabelle `lagerort`
--
ALTER TABLE `lagerort`
MODIFY `lgortid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT für Tabelle `lagerverkehr`
--
ALTER TABLE `lagerverkehr`
MODIFY `lgvid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `lieferant`
--
ALTER TABLE `lieferant`
MODIFY `lfid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `lieferposition`
--
ALTER TABLE `lieferposition`
MODIFY `lpid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT für Tabelle `nachricht`
--
ALTER TABLE `nachricht`
MODIFY `nid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT für Tabelle `produkt`
--
ALTER TABLE `produkt`
MODIFY `prodid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `bestellung`
--
ALTER TABLE `bestellung`
ADD CONSTRAINT `bestellung_ibfk_1` FOREIGN KEY (`kunde`) REFERENCES `kunde` (`kid`);

--
-- Constraints der Tabelle `bestellungsposition`
--
ALTER TABLE `bestellungsposition`
ADD CONSTRAINT `bestellungsposition_ibfk_1` FOREIGN KEY (`bestellung`) REFERENCES `bestellung` (`bid`),
ADD CONSTRAINT `bestellungsposition_ibfk_2` FOREIGN KEY (`produkt`) REFERENCES `produkt` (`prodid`);

--
-- Constraints der Tabelle `einlieferung`
--
ALTER TABLE `einlieferung`
ADD CONSTRAINT `einlieferung_ibfk_2` FOREIGN KEY (`lieferant`) REFERENCES `lieferant` (`lfid`);

--
-- Constraints der Tabelle `lagerort`
--
ALTER TABLE `lagerort`
ADD CONSTRAINT `lagerort_ibfk_1` FOREIGN KEY (`lager`) REFERENCES `lager` (`lagerid`);

--
-- Constraints der Tabelle `lagerverkehr`
--
ALTER TABLE `lagerverkehr`
ADD CONSTRAINT `lagerverkehr_ibfk_1` FOREIGN KEY (`einlieferung`) REFERENCES `einlieferung` (`elfid`),
ADD CONSTRAINT `lagerverkehr_ibfk_2` FOREIGN KEY (`bestellung`) REFERENCES `bestellung` (`bid`),
ADD CONSTRAINT `lagerverkehr_ibfk_3` FOREIGN KEY (`lager`) REFERENCES `lager` (`lagerid`);

--
-- Constraints der Tabelle `lieferposition`
--
ALTER TABLE `lieferposition`
ADD CONSTRAINT `lieferposition_ibfk_1` FOREIGN KEY (`einlieferung`) REFERENCES `einlieferung` (`elfid`),
ADD CONSTRAINT `lieferposition_ibfk_2` FOREIGN KEY (`produkt`) REFERENCES `produkt` (`prodid`);

--
-- Constraints der Tabelle `nachricht`
--
ALTER TABLE `nachricht`
ADD CONSTRAINT `nachricht_ibfk_1` FOREIGN KEY (`kunde`) REFERENCES `kunde` (`kid`);

--
-- Constraints der Tabelle `produkt`
--
ALTER TABLE `produkt`
ADD CONSTRAINT `produkt_ibfk_1` FOREIGN KEY (`kategorie`) REFERENCES `kategorie` (`katid`),
ADD CONSTRAINT `produkt_ibfk_2` FOREIGN KEY (`lagerort`) REFERENCES `lagerort` (`lgortid`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
