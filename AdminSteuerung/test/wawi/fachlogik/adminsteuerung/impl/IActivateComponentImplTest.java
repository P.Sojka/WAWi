package wawi.fachlogik.adminsteuerung.impl;

import javax.persistence.EntityManager;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import wawi.datenhaltung.wawidbmodel.service.EntityManagerSingleton;
import wawi.fachlogik.componentcontroller.service.CompType;

/**
 *
 * @author gromow
 */
public class IActivateComponentImplTest {

    private EntityManager em;
    private IActivateComponentImpl classUnderTest;
    private int adminKey = 9;

    public IActivateComponentImplTest() {
    }

    /*
    *ANGENOMMEN die Implementierung der IActivateComponent Schnittstelle wird als classUnderTest instanziiert,
    *UND der EntityManager wird korrekt geholt, (nicht immer erforderlich),
    *UND die Transaktion von em wird gestartet, (nicht immer erforderlich).
     */
    @Before
    public void angenommen() {
        EntityManagerSingleton ems = EntityManagerSingleton.getInstance();
        ems.useDevPU();
        EntityManager em = ems.getEntityManager();
        classUnderTest = new IActivateComponentImpl();
//        em.getTransaction().begin();
    }

    @After
    public void amEnde() {
//        em.getTransaction().rollback();
//        em.getTransaction().commit();
    }

    /**
     * WENN die Methode getCompType aufgerufen wird, DANN sollte sie den
     * entsprechenden CompType der Komponente zurückliefern.
     */
    @Test
    public void getCompType() {
        CompType ct = classUnderTest.getComponentType();
        assertSame(ct, CompType.ADMIN);
    }

    /**
     * WENN die Methode activateComponent aufgerufen wird, UND die Komponente
     * sich im Zustand deactivated befindet, UND für die jeweilige Komponente
     * eine gültige ID übergeben wird, DANN sollte sie TRUE zurückliefern, UND
     * der Zustand in activated gewechselt sein.
     */
    @Test
    public void activateComponent_00() {

        //Defaultmäßig deaktiviert
        boolean istWert = classUnderTest.activateComponent(adminKey);//falls id 9 dann wird aktiviert
        assertTrue(istWert);

    }

    /**
     * WENN die Methode activateComponent aufgerufen wird, UND die Komponente
     * sich im Zustand activated befindet, UND für die jeweilige Komponente eine
     * gültige ID übergeben wird, DANN sollte sie FALSE zurückliefern, UND der
     * Zustand in activated bleiben.
     */
    @Test
    public void activateComponent_01() {
        //Somit aktiviere ich meine Komponente 
        classUnderTest.getComponentType();
        boolean sollWert = classUnderTest.activateComponent(adminKey);
        assertFalse(sollWert);
    }

    /**
     * WENN die Methode activateComponent aufgerufen wird, UND die Komponente
     * sich im Zustand deactivated befindet, UND für die jeweilige Komponente
     * eine ungültige ID übergeben wird, DANN sollte sie FALSE zurückliefern,
     * UND der Zustand in deactivated bleiben.
     */
    @Test
    public void activateComponent_02() {
        //Defaultmäßig deaktiviert
        boolean sollWert = classUnderTest.activateComponent(adminKey + 1);
        assertFalse(sollWert);
    }

    /**
     * WENN die Methode activateComponent aufgerufen wird, UND die Komponente
     * sich im Zustand activated befindet, UND für die jeweilige Komponente eine
     * ungültige ID übergeben wird, DANN sollte sie FALSE zurückliefern, UND der
     * Zustand in deactivated bleiben.
     */
    @Test
    public void activateComponent_03() {
        //Somit aktiviere ich meine Komponente 
        classUnderTest.getComponentType();
        boolean sollWert = classUnderTest.activateComponent(adminKey + 1);
        assertFalse(sollWert);
    }

    /**
     * WENN die Methode deactivateComponent aufgerufen wird, UND die Komponente
     * sich im Zustand activated befindet, DANN sollte sie TRUE zurückliefern,
     * UND der Zustand in deactivated gewechselt sein.
     */
    @Test
    public void deactivateComponent_00() {
        classUnderTest.getComponentType();
        boolean sollWert = classUnderTest.deactivateComponent();
        assertTrue(sollWert);
    }

    /**
     * WENN die Methode deactivateComponent aufgerufen wird, UND die Komponente
     * sich im Zustand deactivated befindet, DANN sollte sie FALSE
     * zurückliefern, UND der Zustand in deactivated bleiben.
     */
    @Test
    public void deactivateComponent_01() {
        boolean sollWert = classUnderTest.deactivateComponent();
        assertFalse(sollWert);
        assertEquals(classUnderTest.isActivated(), false);
    }

    /**
     * WENN die Methode isActivated aufgerufen wird, UND die Komponente
     * aktiviert ist, DANN sollte sie TRUE zurückliefern.
     */
    @Test
    public void isActivated_00() {
        classUnderTest.getComponentType();
        boolean sollWert = classUnderTest.isActivated();
        assertTrue(sollWert);
    }

    /**
     * WENN die Methode isActivated aufgerufen wird, UND die Komponente
     * deaktiviert ist, DANN sollte sie FALSE zurückliefern.
     */
    @Test
    public void isActivated_01() {
        //Defaultmäßig wird eine Komponente deaktiviert
        assertFalse(classUnderTest.isActivated());
    }

}
