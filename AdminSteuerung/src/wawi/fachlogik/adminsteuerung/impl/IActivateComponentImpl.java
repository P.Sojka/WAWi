package wawi.fachlogik.adminsteuerung.impl;

import wawi.fachlogik.componentcontroller.service.CompType;
import wawi.fachlogik.componentcontroller.service.IActivateComponent;

/**
 *
 * @author Patrick-Sojka
 */
public class IActivateComponentImpl implements IActivateComponent {

    public boolean activated;
    private CompType ct;

    public IActivateComponentImpl() {
        activated = false;
    }

    /**
     * Hier wird die Methode getComponentType ausprogrammiert.Diese liefert ein
     * gültigen Komponenten Schlüssel.
     *
     * @return ct
     */
    @Override
    public CompType getComponentType() {

        ct = CompType.ADMIN;
        return ct;

    }

    /**
     * Hier wird die activateComponent ausprogrammiert.Hier wird die Komponente
     * mit gültiger id aktiviert falls sie vorher deaktiviert war.
     *
     * @param userid
     * @return activated
     */
    @Override
    public boolean activateComponent(int userid) {
        if (isActivated() == false) {
            if (userid == 9) {
                activated = true;
                return true;
            }
        }
        if (isActivated() == true) {
            return false;
        }
        if (userid != 9) {
            return false;
        }
        return activated;
    }

    /**
     * Hier wird die Metode deactivateComponent ausprogrammiert.Hier wird die
     * componente deaktiviert wenn sie vorher aktiviert war.
     *
     * @return activated
     */
    @Override
    public boolean deactivateComponent() {
        //System.out.println("deactivateComp isActivated schon standard: " + isActivated());

        if (isActivated() == true) {
            activated = false;
            return true;
        }
        if (isActivated() == false) {
            activated = false;
            return false;
        }
        return activated;
    }

    /**
     * Hier wird die Methode isActivated ausprogrammiert.Hier wird geprüft ob
     * die Komponente aktiviert oder deaktiviert ist.
     *
     * @return activated
     */
    @Override
    public boolean isActivated() {

        if (activated == true) {
            ct = getComponentType();
            return true;
        }
        if (activated == false) {
            ct = null;
            return false;
        }
        return true;

    }
}
