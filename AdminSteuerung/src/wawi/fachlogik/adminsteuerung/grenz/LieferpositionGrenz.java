/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.fachlogik.adminsteuerung.grenz;

import java.math.BigDecimal;

/**
 *
 * @author gromow
 */
public class LieferpositionGrenz {
    private int lpid;
    private int einlieferung;
    private int produkt;
    private int anzahl;
    private BigDecimal kaufpreis;

    public LieferpositionGrenz(int lpid, int einlieferung, int produkt, int anzahl, BigDecimal kaufpreis) {
        this.lpid = lpid;
        this.einlieferung = einlieferung;
        this.produkt = produkt;
        this.anzahl = anzahl;
        this.kaufpreis = kaufpreis;
    }

    public int getAnzahl() {
        return anzahl;
    }

    public int getEinlieferung() {
        return einlieferung;
    }

    public BigDecimal getKaufpreis() {
        return kaufpreis;
    }

    public int getLpid() {
        return lpid;
    }

    public int getProdukt() {
        return produkt;
    }
    
    
    
}
