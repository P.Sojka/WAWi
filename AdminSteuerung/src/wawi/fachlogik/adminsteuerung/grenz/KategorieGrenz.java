/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.fachlogik.adminsteuerung.grenz;

/**
 *
 * @author gromow
 */
public class KategorieGrenz {
    private int katid;
    private int parentkatid;
    private String name;
    private String beschreibung;

    public KategorieGrenz() {
    }

    public KategorieGrenz(int katid, int parentkatid, String name, String beschreibung) {
        this.katid = katid;
        this.parentkatid = parentkatid;
        this.name = name;
        this.beschreibung = beschreibung;
    }

    public int getKatid() {
        return katid;
    }

    public void setKatid(int katid) {
        this.katid = katid;
    }

    public int getParentkatid() {
        return parentkatid;
    }

    public void setParentkatid(int parentkatid) {
        this.parentkatid = parentkatid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }
    
    
}
