/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.fachlogik.adminsteuerung.grenz;

import java.math.BigDecimal;
import java.util.Date;

/**
 *
 * @author gromow
 */
public class BestellungGrenz {

    private int bid;
    private String lieferadresse;
    private String rechnungsadresse;
    private Date created;
    
    private String kunde;

    private String status;
    
    private BigDecimal gesamtnetto;
    private BigDecimal gesamtbrutto;

    public BestellungGrenz() {
    }

    public BestellungGrenz(int bid,String kunde, String lieferadresse, String rechnungsadresse,String status, Date created, BigDecimal gesamtnetto, BigDecimal gesamtbrutto) {
        this.bid = bid;
        this.kunde = kunde;
        this.lieferadresse = lieferadresse;
        this.rechnungsadresse = rechnungsadresse;
        this.created = created;
        this.gesamtnetto = gesamtnetto;
        this.gesamtbrutto = gesamtbrutto;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setKunde(String kunde) {
        this.kunde = kunde;
    }

    public String getKunde() {
        return kunde;
    }

    

    
    
    public int getBid() {
        return bid;
    }

    public Date getCreated() {
        return created;
    }

    public BigDecimal getGesamtbrutto() {
        return gesamtbrutto;
    }

    public BigDecimal getGesamtnetto() {
        return gesamtnetto;
    }

    public String getLieferadresse() {
        return lieferadresse;
    }

    public String getRechnungsadresse() {
        return rechnungsadresse;
    }

    public void setBid(int bid) {
        this.bid = bid;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setGesamtbrutto(BigDecimal gesamtbrutto) {
        this.gesamtbrutto = gesamtbrutto;
    }

    public void setGesamtnetto(BigDecimal gesamtnetto) {
        this.gesamtnetto = gesamtnetto;
    }

    public void setLieferadresse(String lieferadresse) {
        this.lieferadresse = lieferadresse;
    }

    public void setRechnungsadresse(String rechnungsadresse) {
        this.rechnungsadresse = rechnungsadresse;
    }
    
    
}
