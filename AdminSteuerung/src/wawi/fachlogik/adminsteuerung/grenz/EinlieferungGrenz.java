/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.fachlogik.adminsteuerung.grenz;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 *
 * @author gromow
 */
public class EinlieferungGrenz {
    private int elfid;
    private int lieferant;
    private Date created;
    private BigDecimal gesamtpreis;
    private List<LieferpositionGrenz> lipos;

    public EinlieferungGrenz() {
    }

    public EinlieferungGrenz(int elfid, int lieferant, Date created, BigDecimal gesamtpreis, List<LieferpositionGrenz> lipos) {
        this.elfid = elfid;
        this.lieferant = lieferant;
        this.created = created;
        this.gesamtpreis = gesamtpreis;
        this.lipos = lipos;
    }



    

    public void setLipos(List<LieferpositionGrenz> lipos) {
        this.lipos = lipos;
    }

    public List<LieferpositionGrenz> getLipos() {
        return lipos;
    }

    

    

    

    public Date getCreated() {
        return created;
    }

    public int getElfid() {
        return elfid;
    }

    public BigDecimal getGesamtpreis() {
        return gesamtpreis;
    }

    

    public int getLieferant() {
        return lieferant;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setElfid(int elfid) {
        this.elfid = elfid;
    }

    public void setGesamtpreis(BigDecimal gesamtpreis) {
        this.gesamtpreis = gesamtpreis;
    }

   

    public void setLieferant(int lieferant) {
        this.lieferant = lieferant;
    }

    
    
    
}
