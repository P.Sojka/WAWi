/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.fachlogik.adminsteuerung.grenz;

import java.util.List;

/**
 *
 * @author gromow
 */
public class LagerortGrenz {

    private int lgortid;
    private String bezeichnung;
    private int kapazitaet;

    public LagerortGrenz() {
    }

    public LagerortGrenz(int lgortid, String bezeichnung, int kapazitaet) {
        this.lgortid = lgortid;
        this.bezeichnung = bezeichnung;
        this.kapazitaet = kapazitaet;
    }

    public String getBezeichnung() {
        return bezeichnung;
    }

    public int getKapazitaet() {
        return kapazitaet;
    }

    public int getLgortid() {
        return lgortid;
    }

    public void setBezeichnung(String bezeichnung) {
        this.bezeichnung = bezeichnung;
    }

    public void setKapazitaet(int kapazitaet) {
        this.kapazitaet = kapazitaet;
    }

    public void setLgortid(int lgortid) {
        this.lgortid = lgortid;
    }

}
