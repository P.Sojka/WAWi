/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.fachlogik.adminsteuerung.grenz;

import java.math.BigDecimal;
import java.util.Date;


/**
 *
 * @author gromow
 */
public class LagerverkehrGrenz {
    private int lgvid;
    private int lager;
    private int einlieferung;
    private int bestellung;
    private Date created;
    private String status;
    private BigDecimal gesamtpreis;

    public LagerverkehrGrenz() {
    }

    public LagerverkehrGrenz(int lgvid, int lager, int einlieferung, int bestellung, Date created, String status, BigDecimal gesamtpreis) {
        this.lgvid = lgvid;
        this.lager = lager;
        this.einlieferung = einlieferung;
        this.bestellung = bestellung;
        this.created = created;
        this.status = status;
        this.gesamtpreis = gesamtpreis;
    }

    







    public BigDecimal getGesamtpreis() {
        return gesamtpreis;
        
    }

    public void setGesamtpreis(BigDecimal gesamtpreis) {
        this.gesamtpreis = gesamtpreis;
    }

    public void setBestellung(int bestellung) {
        this.bestellung = bestellung;
    }

    public int getBestellung() {
        return bestellung;
    }


    public Date getCreated() {
        return created;
    }



    public int getLager() {
        return lager;
    }

    public int getLgvid() {
        return lgvid;
    }

    public String getStatus() {
        return status;
    }

    public void setEinlieferung(int einlieferung) {
        this.einlieferung = einlieferung;
    }

    public int getEinlieferung() {
        return einlieferung;
    }





    public void setCreated(Date created) {
        this.created = created;
    }



    public void setLager(int lager) {
        this.lager = lager;
    }

    public void setLgvid(int lgvid) {
        this.lgvid = lgvid;
    }

    public void setStatus(String status) {
        this.status = status;
    }
    
    
    
    
}
