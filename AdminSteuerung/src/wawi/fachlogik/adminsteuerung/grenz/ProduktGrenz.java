/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.fachlogik.adminsteuerung.grenz;

import java.math.BigDecimal;
import java.util.Date;


/**
 *
 * @author Patrick-Sojka
 */
public class ProduktGrenz {
    
    int prodid;
    String name;
    String beschreibung;
    Date angelegt;
    int stueckzahl;
    BigDecimal nettopreis;
    int mwstsatz;
    boolean aktiv;
    int kategorie;
    int lagerort;
    
        public ProduktGrenz() {
    }

    public ProduktGrenz(int prodid, String name, String beschreibung, Date angelegt, int stueckzahl, BigDecimal nettopreis, int mwstsatz, boolean aktiv, int kategorie, int lagerort) {
        this.prodid = prodid;
        this.name = name;
        this.beschreibung = beschreibung;
        this.angelegt = angelegt;
        this.stueckzahl = stueckzahl;
        this.nettopreis = nettopreis;
        this.mwstsatz = mwstsatz;
        this.aktiv = aktiv;
        this.kategorie = kategorie;
        this.lagerort = lagerort;
    }
    
    

    public int getProdid() {
        return prodid;
    }

    public boolean getAktiv(){
    return aktiv;
    }
    
    public String getName() {
        return name;
    }

    public String getBeschreibung() {
        return beschreibung;
    }

    public Date getAngelegt() {
        return angelegt;
    }

    public int getStueckzahl() {
        return stueckzahl;
    }

    public BigDecimal getNettopreis() {
        return nettopreis;
    }

    public int getMwstsatz() {
        return mwstsatz;
    }

    public boolean isAktiv() {
        return aktiv;
    }

    public int getKategorie() {
        return kategorie;
    }

    public int getLagerort() {
        return lagerort;
    }

    

    public void setProdid(int prodid) {
        this.prodid = prodid;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBeschreibung(String beschreibung) {
        this.beschreibung = beschreibung;
    }

    public void setAngelegt(Date angelegt) {
        this.angelegt = angelegt;
    }

    public void setStueckzahl(int stueckzahl) {
        this.stueckzahl = stueckzahl;
    }

    public void setNettopreis(BigDecimal nettopreis) {
        this.nettopreis = nettopreis;
    }

    public void setMwstsatz(int mwstsatz) {
        this.mwstsatz = mwstsatz;
    }

    public void setAktiv(boolean aktiv) {
        this.aktiv = aktiv;
    }

    public void setKategorie(int kategorie) {
        this.kategorie = kategorie;
    }

    public void setLagerort(int lagerort) {
        this.lagerort = lagerort;
    }

    


    
    
   
    
   
    
}
