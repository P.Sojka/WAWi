/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.fachlogik.adminsteuerung.steuer;


import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import wawi.datenhaltung.produktverwaltung.impl.ICRUDKategorieImpl;
import wawi.datenhaltung.produktverwaltung.impl.ICRUDProduktImpl;
import wawi.datenhaltung.produktverwaltung.service.ICRUDKategorie;
import wawi.datenhaltung.produktverwaltung.service.ICRUDProdukt;
import wawi.datenhaltung.wawidbmodel.entities.Kategorie;
import wawi.datenhaltung.wawidbmodel.entities.Produkt;
import wawi.datenhaltung.wawidbmodel.impl.IDatabaseImpl;
import wawi.fachlogik.adminsteuerung.grenz.KategorieGrenz;

/**
 *
 * @author gromow
 */
public class KategorieSteuerung {

    private EntityManager em;
    private IDatabaseImpl IDataImpl;

    /**
     * Eine neue Kategorie kann angelegt werden.Die KategorieID muss null
     * sein.Über das GUI kann diese Methode direkt aufgerufen werden.Die
     * Übergabe wird durch eine Grenzklasse realisiert.Der direkter Zugriff auf
     * Kategorie-Entitätsklasse ist nicht möglich.
     *
     * @param produktkategorie
     * @return ret
     */
    public boolean kategorieAnlegen(KategorieGrenz produktkategorie) {

        boolean ret = false;

        ICRUDKategorie ick = new ICRUDKategorieImpl();

        IDataImpl = new IDatabaseImpl(); 
        IDataImpl.useProdPU();
        em = IDataImpl.getEntityManager();

        ick.setEntityManager(em);

        Kategorie k = new Kategorie();
        k.setKatid(null);
        k.setParentkatid(produktkategorie.getParentkatid());
        k.setName(produktkategorie.getName());
        k.setBeschreibung(produktkategorie.getBeschreibung());

        em.getTransaction().begin();
        //To Do : Ab hier try catch ist ein must have
        ret = ick.insertKategorie(k);
        em.getTransaction().commit();

        //Ist gedacht für das Label ob erfolgreich geschrieben wurde oder nicht.
        return ret;
    }

    /**
     * Eine bereits existierende Kategorie kann bearbeitet werden.Das GUI
     * übergibt eine be- arbeitete Kategorie mit der bestehenden KategorieID in
     * form einer Grenzklasse.Die Methode kategorieBearbeiten übergibt die
     * Entitätsklasse vom Typ Kategorie weiter an die ICRUDKategorieImpl, die
     * dann in der Datenbank das Update durchführt.
     *
     * @param produktkategorie
     * @return ret
     */
    public boolean kategorieBearbeiten(KategorieGrenz produktkategorie) {

        boolean ret = false;

        ICRUDKategorie ick = new ICRUDKategorieImpl();
        Kategorie k = new Kategorie();

        k.setKatid(produktkategorie.getKatid());
        k.setParentkatid(produktkategorie.getParentkatid());
        k.setName(produktkategorie.getName());
        k.setBeschreibung(produktkategorie.getBeschreibung());

        IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        em = IDataImpl.getEntityManager();

        ick.setEntityManager(em);

        em.getTransaction().begin();

        ret = ick.updateKategorie(k);

        em.getTransaction().commit();
        
        return ret;
    }

    /**
     * Diese Methode löscht ein Object aus der Datenbank mit Hilfe einer
     * ICRUDKategorieImpl Klasse.Als Parameter reicht die ID-Nummer der zu
     * löschende Kategorie.Dabei wird noch geprüft,ob zu löschende Kategorie
     * noch Produkte besitzt
     *
     * @param katid
     * @return ret
     */
    public boolean kategorieLoeschen(int katid) {
        boolean ret = true;

        ICRUDKategorie ick = new ICRUDKategorieImpl();
        

        IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        em = IDataImpl.getEntityManager();

        ick.setEntityManager(em);
        

        Kategorie kat = ick.getKategorieById(katid);
        int parentkatid = kat.getParentkatid();//2

        /* BUG 380 beseitigen ENDE*/
        /* Das Löschen von Kategorien soll nur dann möglich sein, wenn die zu löschende Kategorie keine Produkte enthält. 
        Ist aber eine Widerspruch zu dem WAWI-Integrationstest Fall 44 */
        ICRUDProdukt icp = new ICRUDProduktImpl();
        icp.setEntityManager(em);
//        List<Produkt> prodlist = icp.getProduktListe();
//        Iterator<Produkt> produktIterator = prodlist.iterator();
//        while (produktIterator.hasNext()) {
//
//            Produkt p = produktIterator.next();
//
//            if (p.getKategorie().getKatid() == katid) {
//                ret = false;
//            }
//        }
        
        /* WAWI - Integrationstest - Fall 44 */
        if(ret && (kat.getParentkatid()!=0 || kat.getKatid()!= kat.getParentkatid())){
            List<Produkt> produktList =  icp.getProduktListe();
            Iterator<Produkt> produktIterator2 = produktList.iterator();
            while(produktIterator2.hasNext()){
                Produkt p = produktIterator2.next();
                if(p.getKategorie().getKatid()==kat.getKatid()){
                    /* Hole neue OberKategorie */
                    Kategorie neueKategorie = ick.getKategorieById(kat.getParentkatid());
                    /* Hole neue Oberkat Ende */
                    p.setKategorie(neueKategorie);
                    em.getTransaction().begin();
                    icp.updateProdukt(p);
                    em.getTransaction().commit();
                }
            }
        }
        /* WAWI - Integrationstest - Fall 44 */

        /* Unterkategorien der zu löschenden Kategorie werden der übergeordneten Kategorie, wenn vorhanden, zugeordnet werden. */
        if (ret) {
            List<Kategorie> katliste = ick.getKategorieListe();
            Iterator<Kategorie> iteratorKategorie = katliste.iterator();
            while (iteratorKategorie.hasNext()) {
                kat = iteratorKategorie.next();
                if (kat.getParentkatid() == katid) {
                    kat.setParentkatid(parentkatid);
                    em.getTransaction().begin();
                    ick.updateKategorie(kat);
                    em.getTransaction().commit();
                }
            }

            em.getTransaction().begin();
            ret = ick.deleteKategorie(katid);
            em.getTransaction().commit();
        }

        return ret;
    }

    /**
     * Es wird nach eine bestimmte Kategorie anhand der richtigen ID-Nummer
     * gesucht.
     *
     * @param katid
     * @return kg
     */
    public KategorieGrenz kategorieFind(int katid) {
        Kategorie k;

        ICRUDKategorie ick = new ICRUDKategorieImpl();

        IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        em = IDataImpl.getEntityManager();

        ick.setEntityManager(em);

        
        k = ick.getKategorieById(katid);
        

        KategorieGrenz kg = new KategorieGrenz(k.getKatid(), k.getParentkatid(), k.getName(), k.getBeschreibung());

        return kg;
    }

    /**
     * Die gesamte KategorieGrenz-Liste wird zurückgeliefert. Diese Methode wird
     * in vielen LFs benutzt. z.B. um eine TreeModel aufzustellen benötigt man
     * alle Kategorien sowohl Ober- als auch Unterkategorien.
     *
     * @return klistgrenz
     */
    public List<KategorieGrenz> kategorieAnzeigen() {
        List<KategorieGrenz> klistgrenz = new ArrayList<>();
        List<Kategorie> katlist = new ArrayList<>();

        ICRUDKategorie ickat = new ICRUDKategorieImpl();

        IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        em = IDataImpl.getEntityManager();

        ickat.setEntityManager(em);

        
        katlist = ickat.getKategorieListe();
        

        Iterator<Kategorie> kategorieIterator = katlist.iterator();
        while (kategorieIterator.hasNext()) {
            Kategorie k = kategorieIterator.next();
            KategorieGrenz kg = new KategorieGrenz();

            kg.setKatid(k.getKatid());
            kg.setName(k.getName());
            kg.setBeschreibung(k.getBeschreibung());
            kg.setParentkatid(k.getParentkatid());

            klistgrenz.add(kg);
        }

        return klistgrenz;
    }
}
