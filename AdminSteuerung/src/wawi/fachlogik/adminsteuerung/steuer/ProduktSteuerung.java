package wawi.fachlogik.adminsteuerung.steuer;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import wawi.datenhaltung.bestellungverwaltungs.impl.IBestellungAdminImpl;
import wawi.datenhaltung.bestellungverwaltungs.service.IBestellungAdmin;
import wawi.datenhaltung.lagerverwaltung.impl.ILagerServiceImpl;
import wawi.datenhaltung.lagerverwaltung.service.ILagerService;
import wawi.datenhaltung.produktverwaltung.impl.ICRUDKategorieImpl;
import wawi.datenhaltung.produktverwaltung.impl.ICRUDProduktImpl;
import wawi.datenhaltung.produktverwaltung.service.ICRUDKategorie;
import wawi.datenhaltung.produktverwaltung.service.ICRUDProdukt;
import wawi.datenhaltung.wawidbmodel.entities.Bestellung;
import wawi.datenhaltung.wawidbmodel.entities.Bestellungsposition;
import wawi.datenhaltung.wawidbmodel.entities.Kategorie;
import wawi.datenhaltung.wawidbmodel.entities.Lagerort;
import wawi.datenhaltung.wawidbmodel.entities.Produkt;
import wawi.datenhaltung.wawidbmodel.impl.IDatabaseImpl;
import wawi.fachlogik.adminsteuerung.grenz.ProduktGrenz;

/**
 *
 * @author Patrick-Sojka
 */
public class ProduktSteuerung {

    private static IDatabaseImpl IDataImpl;
    private static EntityManager em;
    private List<ProduktGrenz> produktgrenzlist = new ArrayList<ProduktGrenz>();

    /**
     * Diese Methode bekommt von der Gui ein Objekt der Klasse ProduktGrenz um
     * ein neues Produkt anzulegen . Wichtig ist, dass die ID-Nummer auf null
     * gesetzt wird da dem Produkt automatisch eine id zugewiesen wird bevor der
     * Datensatz in die Datenbank geschrieben wird. Es ist auch wichtig das
     * ,dass an die Methode übergebende Objekt alle Attribute ein wert
     * zugewiesen bekommen hat und eine Verknüpfung mit den Objekten der Klassen
     * Kategorie und Lagerort vorhanden sind da die Methode ansonsten ein false
     * zur\'fcck gibt. Der Status ist davon nicht betroffen da er ein boolean
     * wert enthält und somit immer mit false vor belastet ist . Wenn alles
     * richtig gesetzt wurde wird alles einem Objekt der Klasse Produkt
     * übergeben und dieses Produkt wird an die ICRUDProduktImpl weiter gegeben
     * die dann das neue Produkt in die Datenbank speichert
     *
     * @param produktgrenz
     * @return ret
     */
    public boolean produktAnlegen(ProduktGrenz produktgrenz) {

        boolean ret = false;

        ICRUDProdukt ic = new ICRUDProduktImpl();
        Produkt produkt = new Produkt();

        IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        em = IDataImpl.getEntityManager();

        produkt.setProdid(null);
        produkt.setName(produktgrenz.getName());
        produkt.setBeschreibung(produktgrenz.getBeschreibung());
        produkt.setAngelegt(produktgrenz.getAngelegt());
        produkt.setStueckzahl(produktgrenz.getStueckzahl());
        produkt.setNettopreis(produktgrenz.getNettopreis());
        produkt.setMwstsatz(produktgrenz.getMwstsatz());
        produkt.setAktiv(produktgrenz.isAktiv());

        ICRUDKategorie ick = new ICRUDKategorieImpl();
        ick.setEntityManager(em);
        em.getTransaction().begin();
        Kategorie k = ick.getKategorieById(produktgrenz.getKategorie());
        em.getTransaction().commit();

        produkt.setKategorie(k);

        ILagerService ils = new ILagerServiceImpl();

        ils.setEntityManager(em);
        em.getTransaction().begin();
        Lagerort lgo = ils.getLagerortById(produktgrenz.getLagerort());
        em.getTransaction().commit();

        produkt.setLagerort(lgo);

        java.sql.Date sqlDate = new java.sql.Date(new Date().getTime());
        produkt.setAngelegt(sqlDate);

        ic.setEntityManager(em);
        em.getTransaction().begin();
        ret = ic.insertProdukt(produkt);
        em.getTransaction().commit();

        return ret;
    }

    /**
     * Diese Methode bekommt von der Gui ein Objekt der Klasse ProduktGrenz um
     * ein Produkt zu bearbeiten. Dabei ist die ID-Nummer schon gesetzt um das
     * richtige Produkt aus der Datenbank über ProduktGrenz an die Methode zu
     * überliefern. Hier bei ist es wieder wichtig das alle Attribute mit Werten
     * belegt sind und die Objekte Kategorie und Lagerort richtig verknüpft
     * werden wenn was geändert wurde. Ist alles beim bearbeiten richtig gesetzt
     * wird alles einem Objekt der Klasse Produkt übergeben und dieses wird dann
     * an die ICRUDProduktImpl übergeben die dann ein update in der Datenbank
     * macht.
     *
     * @param produktgrenz
     * @return ret
     */
    public boolean produktBearbeiten(ProduktGrenz produktgrenz) {

        int pid;
        boolean ret = false;

        ICRUDProdukt ic = new ICRUDProduktImpl();
        Produkt produkt = new Produkt();

        IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        em = IDataImpl.getEntityManager();

        pid = produktgrenz.getProdid();

        produkt.setProdid(pid);
        produkt.setName(produktgrenz.getName());
        produkt.setBeschreibung(produktgrenz.getBeschreibung());
        produkt.setAngelegt(produktgrenz.getAngelegt());
        produkt.setStueckzahl(produktgrenz.getStueckzahl());
        produkt.setNettopreis(produktgrenz.getNettopreis());
        produkt.setMwstsatz(produktgrenz.getMwstsatz());
        produkt.setAktiv(produktgrenz.isAktiv());

        ICRUDKategorie ick = new ICRUDKategorieImpl();
        ick.setEntityManager(em);
        em.getTransaction().begin();
        Kategorie k = ick.getKategorieById(produktgrenz.getKategorie());
        em.getTransaction().commit();
        produkt.setKategorie(k);

        ILagerService ils = new ILagerServiceImpl();
        ils.setEntityManager(em);
        em.getTransaction().begin();
        Lagerort lgo = ils.getLagerortById(produktgrenz.getLagerort());
        em.getTransaction().commit();
        produkt.setLagerort(lgo);

        produkt.setAngelegt(produktgrenz.getAngelegt());

        ic.setEntityManager(em);
        em.getTransaction().begin();
        ret = ic.updateProdukt(produkt);
        em.getTransaction().commit();

        return ret;
    }

    /**
     * Diese Methode bekommt von der Gui ein ganzzahliegen Wert von dem Objekt
     * ProduktGrenz der die ID-Nummer darstellt des zu löschenden Produktes.
     * Dabei wird die ID einfach an die ICRUDProduktImpl übergeben die dann das
     * Produkt löscht!
     *
     * @param produktgrenz
     * @return ret
     */
    public boolean produktLoeschen(int produktgrenz) {

          boolean ret = true;

        ICRUDProdukt ic = new ICRUDProduktImpl();

        IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        em = IDataImpl.getEntityManager();
        ic.setEntityManager(em);

        /**
         * Prüfen ob die ProduktID bereit in der Bestellungsposition Tabelle
         * vorhanden ist
         */
        IBestellungAdmin iba = new IBestellungAdminImpl();
        iba.setEntityManager(em);
        List<Bestellung> bestellunglist = iba.getAlleBestellungen();
        Iterator<Bestellung> bestellung_iterator = bestellunglist.iterator();
        while (bestellung_iterator.hasNext()) {
            Bestellung bestellung = bestellung_iterator.next();
            Iterator<Bestellungsposition> bestellungsposition_iterator = bestellung.getBestellungspositionList().iterator();
            while (bestellungsposition_iterator.hasNext()) {
                Bestellungsposition bestellungsposition = bestellungsposition_iterator.next();
                if (bestellungsposition.getProdukt().getProdid() == produktgrenz) {
                    ret = false;
                }
            }
            
        }
        

        if (ret) {
            em.getTransaction().begin();
            ret = ic.deleteProdukt(produktgrenz);
            em.getTransaction().commit();
        }

        return ret;

    }

    //////ANTON////////   
    /**
     * Die Methode "produktAnzeigenByStueckzahl()" zeigt die Liste der Produkte
     * an, nach dem man nach der Stückzahl gefiltert hat. Der Parameter "flag"
     * hierbei spielt eine wichtige Rolle. Mit Hilfe davon wird entschieden, wie
     * genau sortiert werden muss. z.B (wenn man unter der eingegebenen
     * Stückzahl die Produkte anzeigen möchte, dann ist der flag "kleiner als")
     * Mit Hilfe der Methode "getProduktListe" der Klasse ICRUDProduktImpl
     * bekommt man eine komplette Liste der Produkte zurück. Diese Liste wird in
     * "produktlist" gespeichert. Diese Liste "produktlist" wird durchgegangen
     * und nachher in einem Objekt "hilf" der Klasse ProduktGrenz
     * zwischengespeichert. Nach der Filterung der flag-Option wird in die Liste
     * "produktgrenzlist" das jeweilige "hilf"-Objekt reingeschrieben.
     *
     * @param stueckzahl
     * @param flag
     * @return produktgrenzliste
     */
    public List<ProduktGrenz> produktAnzeigenByStueckzahl(int stueckzahl, String flag) {

        ICRUDProdukt icrudprodukt = new ICRUDProduktImpl();

        IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        em = IDataImpl.getEntityManager();

        icrudprodukt.setEntityManager(em);
        em.getTransaction().begin();

        List<Produkt> produktlist = icrudprodukt.getProduktListe();

        em.getTransaction().commit();

        for (Produkt p : produktlist) {

            ProduktGrenz hilf = new ProduktGrenz();

            hilf.setProdid(p.getProdid());
            hilf.setName(p.getName());
            hilf.setBeschreibung(p.getBeschreibung());
            hilf.setStueckzahl(p.getStueckzahl());
            hilf.setNettopreis(p.getNettopreis());
            hilf.setMwstsatz(p.getMwstsatz());
            hilf.setAktiv(p.getAktiv());
            hilf.setKategorie(p.getKategorie().getKatid()); // kategorie und lagerort sind objekte
            hilf.setLagerort(p.getLagerort().getLgortid());
            hilf.setAngelegt(p.getAngelegt());

            if (flag == "kleiner als") {
                if (p.getStueckzahl() < stueckzahl) {

                    produktgrenzlist.add(hilf);

                }
            }
            if (flag == "groesser als") {
                if (p.getStueckzahl() > stueckzahl) {

                    produktgrenzlist.add(hilf);
                }
            }
            if (flag == "gleich") {
                if (p.getStueckzahl() == stueckzahl) {

                    produktgrenzlist.add(hilf);
                }

            }
        }
        return produktgrenzlist;

    }

    /**
     * Die Methode "produktAnzeigen" gibt die Liste der Produkte aus. Es wird
     * kein Parameter übergeben. Mit Hilfe der Methode "getProduktListe" der
     * Klasse ICRUDProduktImpl bekommt man eine komplette Liste der Produkte
     * zurück. Diese Liste wird in "produktlist" gespeichert. Diese Liste
     * "produktlist" wird durchgegangen und nachher in einem Objekt "hilf" der
     * Klasse ProduktGrenz zwischengespeichert.
     *
     * @return produktgrenzlist
     */
    public List<ProduktGrenz> produktAnzeigen() {

        ICRUDProdukt icrudprodukt = new ICRUDProduktImpl();

        IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        em = IDataImpl.getEntityManager();

        icrudprodukt.setEntityManager(em);
        em.getTransaction().begin();

        List<Produkt> produktlist = icrudprodukt.getProduktListe();

        em.getTransaction().commit();

        for (Produkt p : produktlist) {

            ProduktGrenz hilf = new ProduktGrenz();

            hilf.setProdid(p.getProdid());
            hilf.setName(p.getName());
            hilf.setBeschreibung(p.getBeschreibung());
            hilf.setStueckzahl(p.getStueckzahl());
            hilf.setNettopreis(p.getNettopreis());
            hilf.setMwstsatz(p.getMwstsatz());
            hilf.setAktiv(p.getAktiv());
            hilf.setKategorie(p.getKategorie().getKatid()); // kategorie und lagerort sind objekte
            hilf.setLagerort(p.getLagerort().getLgortid());
            hilf.setAngelegt(p.getAngelegt());

            produktgrenzlist.add(hilf);

        }

        return produktgrenzlist;
    }
/**
 * Die Methode "produktStatusAktiv" setzt den Status der ausgewählten Produktliste
 * oder eines Produkts auf aktiv (true).
 * 
 * @param prodlist
 * @return 
 */
    public boolean produktStatusAktiv(List<ProduktGrenz> prodlist) {

        Iterator<ProduktGrenz> prodlistIterator = prodlist.iterator();

        boolean ret = false;
        IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        em = IDataImpl.getEntityManager();

        ICRUDProdukt icp = new ICRUDProduktImpl();
        ICRUDKategorie ick = new ICRUDKategorieImpl();
        ILagerService ilg = new ILagerServiceImpl();

        ick.setEntityManager(em);
        icp.setEntityManager(em);
        ilg.setEntityManager(em);

        while (prodlistIterator.hasNext()) {

            Produkt p = new Produkt();
            ProduktGrenz pg = prodlistIterator.next();
            pg.setAktiv(true);

            p.setProdid(pg.getProdid());
            p.setName(pg.getName());
            p.setBeschreibung(pg.getBeschreibung());
            p.setStueckzahl(pg.getStueckzahl());
            p.setNettopreis(pg.getNettopreis());
            p.setMwstsatz(pg.getMwstsatz());
            p.setAktiv(pg.getAktiv());

            Kategorie k1 = new Kategorie();
            Lagerort l1 = new Lagerort();

            
            k1 = ick.getKategorieById(pg.getKategorie());
            
            l1 = ilg.getLagerortById(pg.getLagerort());
            

            p.setKategorie(k1);
            p.setLagerort(l1);
            p.setAngelegt(pg.getAngelegt());

            em.getTransaction().begin();
            ret = icp.updateProdukt(p);
            em.getTransaction().commit();
            
        }

        return ret;
    }
/**
 * Die Methode "produktStatusInaktiv" setzt den Status der ausgewählten Produktliste
 * oder eines Produkts auf inaktiv (false).
 * 
 * @param prodlist
 * @return 
 */
    public boolean produktStatusInaktiv(List<ProduktGrenz> prodlist) {
        
        Iterator<ProduktGrenz> prodlistIterator = prodlist.iterator();

        boolean ret = false;
        IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        em = IDataImpl.getEntityManager();

        ICRUDProdukt icp = new ICRUDProduktImpl();
        ICRUDKategorie ick = new ICRUDKategorieImpl();
        ILagerService ilg = new ILagerServiceImpl();

        ick.setEntityManager(em);
        icp.setEntityManager(em);
        ilg.setEntityManager(em);

        while (prodlistIterator.hasNext()) {

            Produkt p = new Produkt();
            ProduktGrenz pg = prodlistIterator.next();
            pg.setAktiv(false);

            p.setProdid(pg.getProdid());
            p.setName(pg.getName());
            p.setBeschreibung(pg.getBeschreibung());
            p.setStueckzahl(pg.getStueckzahl());
            p.setNettopreis(pg.getNettopreis());
            p.setMwstsatz(pg.getMwstsatz());
            p.setAktiv(pg.getAktiv());

            Kategorie k1 = new Kategorie();
            Lagerort l1 = new Lagerort();

            
            k1 = ick.getKategorieById(pg.getKategorie());
            
            l1 = ilg.getLagerortById(pg.getLagerort());
            

            p.setKategorie(k1);
            p.setLagerort(l1);
            p.setAngelegt(pg.getAngelegt());

            em.getTransaction().begin();
            ret = icp.updateProdukt(p);
            em.getTransaction().commit();
            
        }

        return ret;
    }

}
