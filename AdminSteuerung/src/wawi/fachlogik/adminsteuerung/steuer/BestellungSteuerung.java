/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.fachlogik.adminsteuerung.steuer;

import java.text.ParseException;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import wawi.datenhaltung.bestellungverwaltungs.impl.IBestellungAdminImpl;
import wawi.datenhaltung.bestellungverwaltungs.service.IBestellungAdmin;
import wawi.datenhaltung.wawidbmodel.entities.Bestellung;
import wawi.datenhaltung.wawidbmodel.impl.IDatabaseImpl;
import wawi.fachlogik.adminsteuerung.grenz.BestellungGrenz;

/**
 *
 * @author gromow
 */
public class BestellungSteuerung {

    private static List<BestellungGrenz> bestellunggrenzlist;
    private EntityManager em;
    private IDatabaseImpl IDataImpl;

    /**
     * Benötigt man diese Methode um den LF140 zu realisieren. Die Liste der
     * Bestellungen wird in der jScrollPane über die setModel() integriert. Für
     * die benutzung der Methode braucht man die Komponente
     * BestellungVerwaltungS da diese eine Dummy ist.
     *
     * @return bestellungsListe
     */
    public List<BestellungGrenz> bestellungenAnzeigen() {

        IBestellungAdmin iba = new IBestellungAdminImpl();

        this.IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        this.em = IDataImpl.getEntityManager();

        iba.setEntityManager(em);
        List<Bestellung> bestellungsListe = iba.getAlleBestellungen();

        bestellunggrenzlist = new ArrayList<>();
        Iterator<Bestellung> BestellungIterator = bestellungsListe.iterator();
        while (BestellungIterator.hasNext()) {
            BestellungGrenz bestellunggrenz = new BestellungGrenz();

            Bestellung b = BestellungIterator.next();
            bestellunggrenz.setBid(b.getBid());
            bestellunggrenz.setKunde(b.getKunde().getName() + " " + b.getKunde().getVorname());
            bestellunggrenz.setLieferadresse(b.getLieferadresse());
            bestellunggrenz.setStatus(b.getStatus());
            bestellunggrenz.setRechnungsadresse(b.getRechnungsadresse());
            bestellunggrenz.setCreated(b.getCreated());
            bestellunggrenz.setGesamtbrutto(b.getGesamtbrutto());
            bestellunggrenz.setGesamtnetto(b.getGesamtnetto());
            bestellunggrenzlist.add(bestellunggrenz);

        }

        return bestellunggrenzlist;
    }

    /**
     * Eine bestimmte Bestellung wird zurückgeliefert.
     *
     * @param bid
     * @return bestellunggrenz
     */
    public BestellungGrenz bestellungenAnzeigen(int bid) {

        IBestellungAdmin iba = new IBestellungAdminImpl();

        this.IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        this.em = IDataImpl.getEntityManager();

        iba.setEntityManager(em);
        Bestellung b = iba.getBestellungById(bid);

        BestellungGrenz bestellunggrenz = new BestellungGrenz(); //= iba.getBestellungById(bid);
        if (b != null) {
            bestellunggrenz.setBid(b.getBid());
            bestellunggrenz.setKunde(b.getKunde().getName() + " " + b.getKunde().getVorname());
            bestellunggrenz.setCreated(b.getCreated());
            bestellunggrenz.setGesamtbrutto(b.getGesamtbrutto());
            bestellunggrenz.setGesamtnetto(b.getGesamtnetto());
            bestellunggrenz.setLieferadresse(b.getLieferadresse());
            bestellunggrenz.setRechnungsadresse(b.getRechnungsadresse());
            bestellunggrenz.setStatus(b.getStatus());
        } else {
            bestellunggrenz = null;
        }
        return bestellunggrenz;
    }

    /**
     * Die Mehode rechnet zuerst einen plus Tag zum Datum 'bis' denn die DB
     * falsch berechnet. Die Uhrzeit wird beeinflusst. Erst danach liefert uns
     * die überladene Methode eine korrekte ANzahl von DAtensätzen zurück.
     *
     * @param von
     * @param bis
     * @return bestellungsListe
     * @throws ParseException
     */
    public List<BestellungGrenz> bestellungenAnzeigen(Date von, Date bis) throws ParseException {

        //Ein Tag dazuaddieren
        //http://stackoverflow.com/questions/2305973/java-util-date-vs-java-sql-date
        Calendar c = Calendar.getInstance();
        c.setTime(bis);
        c.add(Calendar.DATE, 1);
        bis = c.getTime();
        //liefert sql Dateformat YYYY-MM-DD zurück
        java.sql.Date sqlDateVon = new java.sql.Date(von.getTime());// liefert wieder 2015-08-10
        java.sql.Date sqlDateBis = new java.sql.Date(bis.getTime());// liefert wieder 2015-08-12
        //holt die Liste mit dem Interval

        IBestellungAdmin iba = new IBestellungAdminImpl();

        this.IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        this.em = IDataImpl.getEntityManager();

        iba.setEntityManager(em);
        List<Bestellung> bestellungsListe = iba.getBestellungenByDatum(sqlDateVon, sqlDateBis);

        bestellunggrenzlist = new ArrayList<>();
        Iterator<Bestellung> BestellungIterator = bestellungsListe.iterator();

        while (BestellungIterator.hasNext()) {
            BestellungGrenz bestellunggrenz = new BestellungGrenz();

            Bestellung best = BestellungIterator.next();
            bestellunggrenz.setBid(best.getBid());
            bestellunggrenz.setKunde(best.getKunde().getName() + " " + best.getKunde().getVorname());
            bestellunggrenz.setLieferadresse(best.getLieferadresse());
            bestellunggrenz.setStatus(best.getStatus());
            bestellunggrenz.setRechnungsadresse(best.getRechnungsadresse());
            bestellunggrenz.setCreated(best.getCreated());
            bestellunggrenz.setGesamtbrutto(best.getGesamtbrutto());
            bestellunggrenz.setGesamtnetto(best.getGesamtnetto());

            bestellunggrenzlist.add(bestellunggrenz);

        }

        return bestellunggrenzlist;
    }
}
