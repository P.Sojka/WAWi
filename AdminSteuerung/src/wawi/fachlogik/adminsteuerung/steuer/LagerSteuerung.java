/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package wawi.fachlogik.adminsteuerung.steuer;

import java.math.BigDecimal;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import javax.persistence.EntityManager;
import wawi.datenhaltung.bestellungverwaltungs.impl.IBestellungAdminImpl;
import wawi.datenhaltung.bestellungverwaltungs.service.IBestellungAdmin;
import wawi.datenhaltung.wawidbmodel.impl.IDatabaseImpl;
import wawi.fachlogik.adminsteuerung.grenz.LagerverkehrGrenz;
import wawi.datenhaltung.lagerverwaltung.service.ILagerService;
import wawi.datenhaltung.wawidbmodel.entities.Lagerverkehr;
import wawi.datenhaltung.lagerverwaltung.impl.ILagerServiceImpl;
import wawi.datenhaltung.wawidbmodel.entities.Bestellung;
import wawi.datenhaltung.wawidbmodel.entities.Einlieferung;
import wawi.datenhaltung.wawidbmodel.entities.Lagerort;
import wawi.datenhaltung.wawidbmodel.entities.Lieferposition;
import wawi.fachlogik.adminsteuerung.grenz.EinlieferungGrenz;
import wawi.fachlogik.adminsteuerung.grenz.LagerortGrenz;
import wawi.fachlogik.adminsteuerung.grenz.LieferpositionGrenz;

public class LagerSteuerung {

    private EntityManager em;
    private IDatabaseImpl IDataImpl;
    private List<LagerverkehrGrenz> lvglist;
    private ILagerService ils;
    private Bestellung b;
    private Einlieferung e;

    /* RAKAN */
    /**
     * Eine Lagerverkehrliste wird zurückgeliefert in Form einer Grenzklasse.
     * Diese benötigen wir für den Fall LF120. Dabei wird nach eine kompletten
     * Liste ohne Eingrenzung gesucht.
     *
     * @return
     */
    public List<LagerverkehrGrenz> lagerverkehrAnzeigen() {

        ils = new ILagerServiceImpl(); //Fehlt uns normaleweise

        this.IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        this.em = IDataImpl.getEntityManager();

        ils.setEntityManager(em);

        em.getTransaction().begin();
        List<Lagerverkehr> lvlist = ils.getGesamtLagerverkehr();
        em.getTransaction().commit();

        lvglist = new ArrayList<>();
        Iterator<Lagerverkehr> lagerverkehrIterator = lvlist.iterator();

        while (lagerverkehrIterator.hasNext()) {
            LagerverkehrGrenz lvg = new LagerverkehrGrenz();
            Lagerverkehr lv = lagerverkehrIterator.next();

            lvg.setLgvid(lv.getLgvid());
            lvg.setLager(lv.getLager().getLagerid());
            lvg.setCreated(lv.getCreated());

            BigDecimal gesamtpreis = new BigDecimal("0.00");
            /**
             * ***** Bestellung als Object holen BUG 403 *****
             */

            IBestellungAdmin iba = new IBestellungAdminImpl();
            iba.setEntityManager(em);
            Bestellung be = null;

            try {

                be = iba.getBestellungById(lv.getBestellung().getBid());
                gesamtpreis = lv.getBestellung().getGesamtbrutto();
            } catch (Exception e) {
                be = new Bestellung();
                be.setBid(0);
            }
            /**
             * ***** Bestellung als Object holen BUG 403 ENDE *****
             */

            lvg.setBestellung(be.getBid());

            /**
             * ***** Bestellung als Object holen BUG 403 *****
             */
            ILagerService ils = new ILagerServiceImpl();
            ils.setEntityManager(em);
            Einlieferung el = null;

            try {
                el = ils.getEinlieferungById(lv.getEinlieferung().getElfid());
                gesamtpreis = lv.getEinlieferung().getGesamtpreis();
            } catch (Exception e) {
                el = new Einlieferung();
                el.setElfid(0);
            }
            /**
             * ***** Bestellung als Object holen BUG 403 ENDE *****
             */

            lvg.setEinlieferung(el.getElfid());

            lvg.setGesamtpreis(gesamtpreis);

            lvglist.add(lvg);
        }

        return lvglist;

    }

    /**
     * Die überladene Methode liefert uns nun die Liste der Lagerverkehre mit
     * dem begrenzten Zeitinterval. Damit die Datenbank richtige Datensätze
     * zurückliefert, soll man +1 Tag dazuaddieren.
     *
     * @param von
     * @param bis
     * @return lvglist
     * @throws ParseException
     */
    public List<LagerverkehrGrenz> lagerverkehrAnzeigen(Date von, Date bis) throws ParseException {

        //Ein Tag dazuaddieren
        Calendar c = Calendar.getInstance();
        c.setTime(bis);
        c.add(Calendar.DATE, 1);
        bis = c.getTime();
        java.sql.Date sqlDateVon = new java.sql.Date(von.getTime());// liefert wieder 2015-08-10
        java.sql.Date sqlDateBis = new java.sql.Date(bis.getTime());// liefert wieder 2015-08-12

        ils = new ILagerServiceImpl(); //Fehlt uns normaleweise

        this.IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        this.em = IDataImpl.getEntityManager();

        ils.setEntityManager(em);

        em.getTransaction().begin();

        List<Lagerverkehr> lvlist = ils.getLagerverkehrByDatum(sqlDateVon, sqlDateBis);
        em.getTransaction().commit();

        lvglist = new ArrayList<>();
        Iterator<Lagerverkehr> lagerverkehrIterator = lvlist.iterator();

        while (lagerverkehrIterator.hasNext()) {
            int bid;
            int elfid;
            LagerverkehrGrenz lvg = new LagerverkehrGrenz();
            Lagerverkehr lv = lagerverkehrIterator.next();

            lvg.setLgvid(lv.getLgvid());
            lvg.setLager(lv.getLager().getLagerid());
            lvg.setCreated(lv.getCreated());
            try {
                bid = lv.getBestellung().getBid();
            } catch (NullPointerException e) {
                bid = 0;
            }
            lvg.setBestellung(bid);
            try {
                elfid = lv.getEinlieferung().getElfid();
            } catch (NullPointerException e) {
                elfid = 0;
            }
            lvg.setEinlieferung(elfid);

            lvglist.add(lvg);
        }

        return lvglist;
    }

    /**
     * Die komplette EinlieferungGrenzliste wird zurückgeliefert. Die Liste
     * besteht auf der Liste alle Lieferpositionen.
     *
     * @return einlistgrenz
     * @throws NullPointerException
     */
    public List<EinlieferungGrenz> einlieferungenAnzeigen() throws NullPointerException {
        List<EinlieferungGrenz> einlistgrenz = new ArrayList<>();
        List<LieferpositionGrenz> lieferPositionGrenzList = new ArrayList<>();

        ils = new ILagerServiceImpl();

        this.IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        this.em = IDataImpl.getEntityManager();

        ils.setEntityManager(em);

        List<Einlieferung> einlist = ils.getAlleEinlieferungen();

        Iterator<Einlieferung> einlieferungIterator = einlist.iterator();
        while (einlieferungIterator.hasNext()) {
            Einlieferung einli = einlieferungIterator.next();
            EinlieferungGrenz einligrenz = new EinlieferungGrenz();

            einligrenz.setElfid(einli.getElfid());
            einligrenz.setLieferant(einli.getLieferant().getLfid());
            einligrenz.setCreated(einli.getCreated());
            try {
                einligrenz.setGesamtpreis(einli.getGesamtpreis());
            } catch (NullPointerException e) {
                einligrenz.setGesamtpreis(null);
            }

            Iterator<Lieferposition> lieferpositionIterator = einli.getLieferpositionList().iterator();
            while (lieferpositionIterator.hasNext()) {
                Lieferposition next = lieferpositionIterator.next();
                LieferpositionGrenz liposgrenz = new LieferpositionGrenz(next.getLpid(), next.getEinlieferung().getElfid(), next.getProdukt().getProdid(), next.getAnzahl(), next.getKaufpreis());
                lieferPositionGrenzList.add(liposgrenz);

            }
            einligrenz.setLipos(lieferPositionGrenzList);

            einlistgrenz.add(einligrenz);
        }

        return einlistgrenz;
    }

    /**
     * Die überladene Methode liefert uns nun die Liste der Einlieferungen mit
     * dem begrenzten Zeitinterval. Damit die Datenbank richtige Datensätze
     * zurückliefert, soll man +1 Tag dazu addieren.
     *
     * @param von
     * @param bis
     * @return einlistgrenz
     * @throws ParseException
     */
    public List<EinlieferungGrenz> einlieferungenAnzeigen(Date von, Date bis) throws ParseException {

        //Ein Tag dazuaddieren
        Calendar c = Calendar.getInstance();
        c.setTime(bis);
        c.add(Calendar.DATE, 1);
        bis = c.getTime();

        java.sql.Date sqlDateVon = new java.sql.Date(von.getTime());
        java.sql.Date sqlDateBis = new java.sql.Date(bis.getTime());

        List<EinlieferungGrenz> einlistgrenz = new ArrayList<>();
        List<LieferpositionGrenz> lieferPositionGrenzList = new ArrayList<>();

        ils = new ILagerServiceImpl();

        this.IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        this.em = IDataImpl.getEntityManager();

        ils.setEntityManager(em);

        List<Einlieferung> einlist = ils.getEinlieferungByDatum(sqlDateVon, sqlDateBis);

        Iterator<Einlieferung> einlieferungIterator = einlist.iterator();
        while (einlieferungIterator.hasNext()) {
            Einlieferung einli = einlieferungIterator.next();
            EinlieferungGrenz einligrenz = new EinlieferungGrenz();

            einligrenz.setElfid(einli.getElfid());
            einligrenz.setLieferant(einli.getLieferant().getLfid());
            einligrenz.setCreated(einli.getCreated());
            try {
                einligrenz.setGesamtpreis(einli.getGesamtpreis());
            } catch (NullPointerException e) {
                einligrenz.setGesamtpreis(null);
            }
            Iterator<Lieferposition> lieferpositionIterator = einli.getLieferpositionList().iterator();
            while (lieferpositionIterator.hasNext()) {
                Lieferposition next = lieferpositionIterator.next();
                LieferpositionGrenz liposgrenz = new LieferpositionGrenz(next.getLpid(), next.getEinlieferung().getElfid(), next.getProdukt().getProdid(), next.getAnzahl(), next.getKaufpreis());
                lieferPositionGrenzList.add(liposgrenz);
            }
            einligrenz.setLipos(lieferPositionGrenzList);

            einlistgrenz.add(einligrenz);
        }

        return einlistgrenz;
    }

    /**
     * Es wird nach eine bestimmte Einliegerung gesucht, die ihre eindeutige
     * Nummer. try-catch sind notwendig. Es wird nur ein Objekt der Klasse
     * EinlieferungGrenz zurückgegeben.
     *
     * @param elfid
     * @return einlieferunggrenz
     * @throws NullPointerException
     */
    public EinlieferungGrenz einlieferungenAnzeigen(int elfid) throws NullPointerException {

        List<LieferpositionGrenz> lieferPositionGrenzList = new ArrayList<>();
        ILagerService ils = new ILagerServiceImpl();

        IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        em = IDataImpl.getEntityManager();

        ils.setEntityManager(em);

        Einlieferung einli = ils.getEinlieferungById(elfid);
        EinlieferungGrenz einligrenz = new EinlieferungGrenz();
        //An der Stelle hätte eigentlich LagerTeam NullPointerEx. abfabgen müssen, haben es aber nicht gemacht.
        if (einli != null) {

            einligrenz.setElfid(einli.getElfid());
            einligrenz.setLieferant(einli.getLieferant().getLfid());
            einligrenz.setCreated(einli.getCreated());
            try {
                einligrenz.setGesamtpreis(einli.getGesamtpreis());
            } catch (NullPointerException e) {
                einligrenz.setGesamtpreis(null);
            }
            Iterator<Lieferposition> lieferpositionIterator = einli.getLieferpositionList().iterator();

            while (lieferpositionIterator.hasNext()) {

                Lieferposition next = lieferpositionIterator.next();

                LieferpositionGrenz liposgrenz = new LieferpositionGrenz(next.getLpid(), next.getEinlieferung().getElfid(), next.getProdukt().getProdid(), next.getAnzahl(), next.getKaufpreis());
                lieferPositionGrenzList.add(liposgrenz);
            }

            einligrenz.setLipos(lieferPositionGrenzList);
        }else{
            einligrenz = null;
        }
        return einligrenz;
    }

    /**
     * Eine gesamte Lageortliste in for einer LagerortGrenz - Liste wird
     * zurückgeliefert. Die Methode braucht man damit man z.B. ein Produkt
     * erstellen kann.
     *
     * @return laogrenzlist
     */
    public List<LagerortGrenz> lagerortAnzeigen() {
        List<LagerortGrenz> laogrenzlist = new ArrayList<>();

        ils = new ILagerServiceImpl();

        this.IDataImpl = new IDatabaseImpl();
        IDataImpl.useProdPU();
        this.em = IDataImpl.getEntityManager();

        ils.setEntityManager(em);

        em.getTransaction().begin();
        List<Lagerort> laolist = ils.getLagerortListe();
        em.getTransaction().commit();

        Iterator<Lagerort> lagerortIterator = laolist.iterator();
        while (lagerortIterator.hasNext()) {
            Lagerort lao = lagerortIterator.next();
            LagerortGrenz laogrenz = new LagerortGrenz();

            laogrenz.setLgortid(lao.getLgortid());
            laogrenz.setBezeichnung(lao.getBezeichnung());
            laogrenz.setKapazitaet(lao.getKapazitaet());

            laogrenzlist.add(laogrenz);
        }

        return laogrenzlist;
    }
}
